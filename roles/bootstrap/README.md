
# Ansible Role c2platform.core.bootstrap

This bootstrap role allows you to install any OS or PIP packages and run
arbitrary commands using a single list, `bootstrap_packages`. This role uses
this list to install, update, or remove packages using the
[`ansible.builtin.pip`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/pip_module.html),
[`ansible.builtin.package`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/package_module.html)
modules.

Note that this role is similar to
[`robertdobock.bootstrap`](https://github.com/robertdebock/ansible-role-bootstrap/),
but it's designed to be simpler and more flexible, without relying on a vars
folder. This design choice is intentional, as the
[Ansible community]((https://stackoverflow.com/questions/29127560/whats-the-difference-between-defaults-and-vars-in-an-ansible-role/58078985#58078985).)
has criticized the use of vars folders.

- [Requirements](#requirements)
- [Role Variables](#role-variables)
  - [`bootstrap_packages`](#bootstrap_packages)
  - [`bootstrap_environment`](#bootstrap_environment)
- [Dependencies](#dependencies)
- [Example Playbook](#example-playbook)

## Requirements

<!-- Any pre-requisites that may not be covered by Ansible itself or the role should be mentioned here. For instance, if the role uses the EC2 module, it may be a good idea to mention in this section that the boto package is required. -->

## Role Variables

<!--  A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well. -->

### `bootstrap_packages`

Use this variable to install or update packages using `ansible.builtin.pip` and
`ansible.builtin.package`. The example below shows how to perform a sequence of
operations:

```yaml
bootstrap_packages:
  - name: python3-pip
    type: os
  - name: upgrade pip and setuptools
    cmd: pip3 install --upgrade pip setuptools
    type: cmd
    changed_when: Successfully installed
  - name:
      - setuptools
      - pyOpenSSL==22.0.0
      - psycopg2-binary
      - lxml
```

The `bootstrap_packages` variable can also be a dictionary if you want to take
advantage of Ansible's ability to merge dictionaries Refer to
[Managing Dictionary Merging in C2 Platform Ansible Projects](https://c2platform.org/docs/guidelines/coding/merge/)
for more information. This is similar to they way the `linux_resources` and
`win_resources` variable work, so you can also refer to the Ansible Roles
`c2platform.core.linux` and `c2platform.wincore.win` for more information and
examples on dictionary merging.

### `bootstrap_environment`

Use variable `bootstrap_environment` to pass `environment` variables to `ansible.builtin.pip` module for example to use a web proxy:

```yaml
bootstrap_environment:
  http_proxy: 'http://127.0.0.1:8080'
  https_proxy: 'https://127.0.0.1:8080'
```

## Dependencies

<!--   A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles. -->

## Example Playbook

<!--   Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too: -->

This role is a dependency of the [common](../common/) role so typically it is included by using that role.

```yaml
    - hosts: servers
      roles:
         - { role: c2platform.core.common }
```


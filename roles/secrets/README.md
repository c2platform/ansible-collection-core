# Ansible Role c2platform.core.secrets

The Ansible `c2platform.core.secrets` role empowers you to effectively manage
secrets using **Ansible Vault** within your Ansible projects. This role is
particularly tailored for the context of Red Hat Automation Platform (AAP) and
AWX. For more comprehensive guidance on managing secrets with Ansible Vault in
the context of AAP and AWX, please refer to the
[C2 Platform documentation](https://c2platform.org/en/docs/guidelines/setup/secrets/).

- [Requirements](#requirements)
- [Role Variables](#role-variables)
  - [`secrets_dirs`](#secrets_dirs)
  - [`secrets_become`](#secrets_become)
- [Dependencies](#dependencies)
- [Example Playbook](#example-playbook)

## Requirements

<!-- Any pre-requisites that may not be covered by Ansible itself or the role should be mentioned here. For instance, if the role uses the EC2 module, it may be a good idea to mention in this section that the boto package is required. -->

## Role Variables

<!--  A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well. -->

### `secrets_dirs`

The `secrets_dirs` list is your key to configuring the locations of your vaults.
This role will search these specified locations, and when a vault is found,
Ansible will load the associated variables using the `include_vars` method.

Here's an example of how to define `secrets_dirs`:

```yaml
secrets_dirs:
  - "{{ inventory_dir }}/secret_vars" # ansible cli
  - "{{ inventory_dir }}/project/secret_vars/acceptance" # awx
```

Please bear in mind that when working with AWX, the `inventory_dir` behaves
differently from what you might expect. It may not correspond to the location in
your source control, so use it judiciously.

### `secrets_become`

By default, there should be no need for privilege escalation when reading or
decrypting an Ansible Vault. However, the role allows you to customize this
behavior using the `secrets_become` variable as demonstrated below:

```yaml
secrets_become:
  ansible_become: true
  ansible_become_user:
  ansible_become_password:
  ansible_become_method:
  ansible_become_flags:
```

## Dependencies

<!--   A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles. -->

## Example Playbook

<!--   Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too: -->

```yaml
```

# Ansible Role c2platform.core.yum

Add Yum repositories using [ansible.builtin.yum_repository](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/yum_repository_module.html).

<!-- MarkdownTOC levels="2,3" autolink="true" -->

- [Requirements](#requirements)
- [Role Variables](#role-variables)
- [Dependencies](#dependencies)
- [Example Playbook](#example-playbook)

<!-- /MarkdownTOC -->

## Requirements

<!-- Any pre-requisites that may not be covered by Ansible itself or the role should be mentioned here. For instance, if the role uses the EC2 module, it may be a good idea to mention in this section that the boto package is required. -->

## Role Variables

<!--  A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well. -->

This role uses list `yum_repositories`. For each list item [ansible.builtin.yum_repository](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/yum_repository_module.html) is used to manage Yum repositories.

For example

```yaml
yum_repositories:
  - name: public_ol7_latest
    description: Oracle Linux $releasever Latest ($basearch)
    baseurl: http://public-yum.oracle.com/repo/OracleLinux/OL7/latest/$basearch/
    gpgkey: http://public-yum.oracle.com/RPM-GPG-KEY-oracle-ol7
    gpgcheck: yes
```

will create a file `/etc/yum.repos.d/public_ol7_latest.repo` with following contents

```
[public_ol7_latest]
async = 1
baseurl = http://public-yum.oracle.com/repo/OracleLinux/OL7/latest/$basearch/
gpgcheck = 1
gpgkey = http://public-yum.oracle.com/RPM-GPG-KEY-oracle-ol7
name = Oracle Linux $releasever Latest ($basearch)
```

## Dependencies

<!--   A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles. -->

## Example Playbook

<!--   Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too: -->

See for example play [plays/mw/oracle.yml](https://gitlab.com/c2platform/ansible/-/blob/master/plays/mw/oracle.yml) with configuration [group_vars/oracle/main.yml](https://gitlab.com/c2platform/ansible/-/blob/master/group_vars/oracle/main.yml). Note that this role is default included in the [c2platform.core.common](../common/) role.

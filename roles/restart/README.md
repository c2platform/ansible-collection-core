# Ansible Role c2platform.core.restart (DEPRECATED)

> :warning: This role is deprecated. You should use
> `c2platform.core.manage_service`.

Orchestrate stop, start, restart routines using one list `restart_config`.

[ansible.builtin.wait_for](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/wait_for_module)html

- [Requirements](#requirements)
- [Role Variables](#role-variables)
  - [Unreachable hosts](#unreachable-hosts)
- [Dependencies](#dependencies)
- [Example Playbook](#example-playbook)

## Requirements

<!-- Any pre-requisites that may not be covered by Ansible itself or the role should be mentioned here. For instance, if the role uses the EC2 module, it may be a good idea to mention in this section that the boto package is required. -->

## Role Variables

<!--  A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well. -->

### Unreachable hosts

The default behavior of this role is that start, stop etc routines will be abandoned / will fail whenever *unreachable hosts* are found. If you want to *ignore* and *skip* such hosts use `restart_ignore_unreachable`.

```yaml
restart_ignore_unreachable: true
```

## Dependencies

<!--   A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles. -->

## Example Playbook

<!--   Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too: -->

An example playblook is in the [Ansible example project](https://gitlab.com/c2platform/ansible) see [plays/mgmt/restart.yml](https://gitlab.com/c2platform/ansible/-/blob/master/plays/mgmt/restart.yml). The configuration is in
[group_vars/all/restart.yml](https://gitlab.com/c2platform/ansible/-/blob/master/group_vars/all/restart.yml).

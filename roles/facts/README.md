# Ansible Role c2platform.core.facts

Role to gather facts from hosts using [ansible.builtin.setup](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/setup_module.html). The results are returned as a fact `common_facts_hosts`. This role can be used directly but is more intended to be integrated / called from other roles using `include_role`. An example of this use is in [haproxy role](../haproxy).

<!-- MarkdownTOC levels="2,3" autolink="true" -->

- [Requirements](#requirements)
- [Role Variables](#role-variables)
- [Dependencies](#dependencies)
- [Example](#example)

<!-- /MarkdownTOC -->

## Requirements

<!-- Any pre-requisites that may not be covered by Ansible itself or the role should be mentioned here. For instance, if the role uses the EC2 module, it may be a good idea to mention in this section that the boto package is required. -->

## Role Variables

<!--  A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well. -->

Specify hosts to gather facts using `common_facts_gather_hosts`. A filter can be provided using `common_facts_filter`.

```yaml
common_facts_gather_hosts: "{{ groups['myapps'] }}"
common_facts_filter: 'ansible_eth1'
```

This role is used / integrated in [haproxy role](../haproxy) as follows

```yaml
- name: Gather facts
  include_role:
    name: c2platform.core.facts
    tasks_from: main
  vars:
    common_facts_role_name: haproxy
```

Now - in this haproxy role - facts of servers can be made available using vars

```yaml
haproxy_facts_gather_hosts: "{{ groups['haproxy'] }}"
haproxy_facts_filter: 'ansible_eth1'
```

The facts are made available with dict `haproxy_facts_hosts` as shown in example below.

<details><summary>Example facts gathered</summary>

List `haproxy_facts_hosts` below shows fact `ansible_eth1` gathered from node **c2d-haproxy1**.

```yaml
haproxy_facts_hosts:
-   ansible_facts:
        ansible_eth1:
            active: true
            device: eth1
            ipv4:
                address: 1.1.4.158
                broadcast: 1.1.4.255
                netmask: 255.255.255.0
                network: 1.1.4.0
            ipv6:
            -   address: fe80::216:3eff:febd:5966
                prefix: '64'
                scope: link
            macaddress: 00:16:3e:bd:59:66
            mtu: 1500
            promisc: false
            speed: 10000
            type: ether
    ansible_loop_var: item
    changed: false
    failed: false
    invocation:
        module_args:
            fact_path: /etc/ansible/facts.d
            filter:
            - ansible_eth1
            gather_subset:
            - all
            gather_timeout: 10
    item: c2d-haproxy1
```

Use `common_facts_ignore_unreachable` to ignore hosts that are unreachable.


</details>

## Dependencies

<!--   A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles. -->

## Example

<!--   Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too: -->


# Ansible Role `c2platform.core.cacerts2`

The `c2platform.core.cacerts2` Ansible role is designed to streamline the
creation and management of your own Certificate Authority (CA). It automates the
generation of certificates and facilitates their distribution across nodes via
other C2 Platform roles. This role leverages the `community.crypto` Ansible
collection for its operations.

This role can be integrated into your Ansible setup in two primary ways:

1. **Direct Inclusion in Other Roles:** Include tasks from
   [`tasks/certs.yml`](./tasks/certs.yml) in other roles. Examples include:

   |Role                                                                                                                       |File                                                                                                                              |
   |---------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------|
   |[`c2platform.mw.apache`](https://gitlab.com/c2platform/ansible-collection-mw/-/tree/master/roles/apache?ref_type=heads)    |[`tasks/certs.yml`](https://gitlab.com/c2platform/ansible-collection-mw/-/blob/master/roles/apache/tasks/certs.yml?ref_type=heads)|
   |[`c2platform.mgmt.harbor`](https://gitlab.com/c2platform/ansible-collection-mgmt/-/tree/master/roles/harbor?ref_type=heads)|[`tasks/main.yml`](https://gitlab.com/c2platform/ansible-collection-mgmt/-/blob/master/roles/harbor/tasks/main.yml?ref_type=heads)|
   |[`c2platform.mw.tomcat`](https://gitlab.com/c2platform/ansible-collection-mw/-/tree/master/roles/tomcat?ref_type=heads)    |[`tasks/cert.yml`](https://gitlab.com/c2platform/ansible-collection-mw/-/blob/master/roles/tomcat/tasks/cert.yml?ref_type=heads)  |
   |[`c2platform.mw.microk8s`](https://gitlab.com/c2platform/ansible-collection-mw/-/tree/master/roles/microk8s?ref_type=heads)|[`tasks/main.yml`](https://gitlab.com/c2platform/ansible-collection-mw/-/blob/master/roles/microk8s/tasks/main.yml?ref_type=heads)|
2. **Using the Companion Role:** If direct integration isn't desired, you can
   use the companion role
  [`c2platform.core.cacerts2_client`](../cacerts2_client/).

- [Requirements](#requirements)
- [Role Variables](#role-variables)
  - [CA Server](#ca-server)
    - [`cacerts2_ca_server` and `cacerts2_ca_dir`: Defines the CA Server and the directory for storing certificates.](#cacerts2_ca_server-and-cacerts2_ca_dir-defines-the-ca-server-and-the-directory-for-storing-certificates)
    - [`cacerts2_ca_domain`: Customizes the CA's private key, certificate, and other parameters.](#cacerts2_ca_domain-customizes-the-cas-private-key-certificate-and-other-parameters)
      - [`external_csp_tag`](#external_csp_tag)
    - [`cacerts2_delegate_become`: Facilitates task execution delegated to the CA Server, especially useful for targeting Windows hosts.](#cacerts2_delegate_become-facilitates-task-execution-delegated-to-the-ca-server-especially-useful-for-targeting-windows-hosts)
  - [CA Clients](#ca-clients)
    - [`apache2_cacerts2_certificates`: Configuration for Apache certificates.](#apache2_cacerts2_certificates-configuration-for-apache-certificates)
    - [`harbor_cacerts2_certificates`: Settings for Harbor certificates, including CA deployment.](#harbor_cacerts2_certificates-settings-for-harbor-certificates-including-ca-deployment)
    - [`tomcat_cacerts2_certificates`: Details for distributing certificates to Java KeyStores and managing Java TrustStores.](#tomcat_cacerts2_certificates-details-for-distributing-certificates-to-java-keystores-and-managing-java-truststores)
- [Dependencies](#dependencies)
- [Example Play](#example-play)
- [Additional Resources](#additional-resources)

<!-- TODO document notify -->

## Requirements

<!-- Any pre-requisites that may not be covered by Ansible itself or the role should be mentioned here. For instance, if the role uses the EC2 module, it may be a good idea to mention in this section that the boto package is required. -->

If you want to manage Java KeyStores and TrustStores, Java needs to be
installed. The [`c2platform.core.java`](../java/) role can be utilized for this
purpose. See [Example Play](#example-play) for an implementation reference.


## Role Variables

<!--  A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well. -->

### CA Server

#### `cacerts2_ca_server` and `cacerts2_ca_dir`: Defines the CA Server and the directory for storing certificates.

Certificates are created and stored on your "CA Server" using
`cacerts2_ca_server` and stored in the directory specified by `cacerts2_ca_dir`.

```yaml
cacerts2_ca_server: c2d-rproxy # inventory_hostname
cacerts2_ca_dir: /etc/ownca
```

#### `cacerts2_ca_domain`: Customizes the CA's private key, certificate, and other parameters.

To customize the CA's private key, certificate, and other parameters, utilize
the `cacerts2_ca_domain` dictionary. You can set options like passphrase,
cipher, and specify the types of files to create:

```yaml
cacerts2_ca_domain:
  common_name: c2d
  create: ['key','csr', 'crt', 'p12', 'pem', 'keystore', 'truststore']
  cipher: auto
  passphrase: supersecure # vault
```

These settings will be used by all nodes that need certificates installed so
these needs to be "global vars" for example kept in `groupvars/all.yml` or
`groupvars/all/certs.yml`.


This role uses a plugin module
[set_certificate_facts](../plugins/modules/set_certificate_facts) to enhance
`cacerts2_ca_domain` list with facts used for certificate creation and
deployment. For example `cacerts2_ca_domain` as shown above will be enhanded to:

```yaml
cacerts2_ca_domain:
    cipher: auto
    common_name: c2d
    create:
    - key
    - csr
    - crt
    - p12
    - pem
    - keystore
    crl: /vagrant/.ca/c2d/c2d.crl
    crt: /vagrant/.ca/c2d/c2d.crt
    csr: /vagrant/.ca/c2d/c2d.csr
    dir: /vagrant/.ca/c2d
    key: /vagrant/.ca/c2d/c2d.key
    passphrase: supersecure
```

On the file system this will create for example

```
.ca/
└── c2d
    ├── c2d.crt
    ├── c2d.csr
    └── c2d.key
```

##### `external_csp_tag`

Replace certificates generated by this role with external certificates by
configuring the `cacerts2_ca_domain` variable with the `external_csp_tag`
option.

```yaml
cacerts2_ca_domain:
  common_name: c2
  cipher: auto
  passphrase: "{{ gs_cacerts2_ca_domain_passphrase }}"  # secret see vault
  create: ['key', 'csr', 'crt', 'p12', 'pem', 'keystore']
  external_csp_tag: rws-csp
  backup_crt: true
```
For more information on using this feature see
[Designing a Public Key Infrastructure (PKI) for RWS GIS with Ansible](https://c2platform.org/en/docs/projects/rws/pki).

#### `cacerts2_delegate_become`: Facilitates task execution delegated to the CA Server, especially useful for targeting Windows hosts.

The `cacerts2_delegate_become` variable in Ansible is designed to facilitate the
execution of tasks delegated to the **CA Server**. This boolean variable, which
defaults to `false`, can be set to `true` for enhanced functionality,
particularly when targeting Windows hosts.

### CA Clients

As mentioned above roles that require certificates can create them on the CA
Server by including the tasks from [tasks/certs.yml](./tasks/certs.yml). In this
section will use Ansible roles
[`c2platform.mw.apache`](https://gitlab.com/c2platform/ansible-collection-mw/-/tree/master/roles/apache?ref_type=heads)
, [`c2platform.mgmt.harbor`](https://gitlab.com/c2platform/ansible-collection-mgmt/-/tree/master/roles/harbor?ref_type=heads)
and [`c2platform.mw.tomcat`](https://gitlab.com/c2platform/ansible-collection-mw/-/tree/master/roles/tomcat?ref_type=heads)
to illustrate how this role is utilized to create, manage and deploy SSL/TLS
certificates, private keys, Java KeyStores and Java TrustStores.

#### `apache2_cacerts2_certificates`: Configuration for Apache certificates.


has a file `tasks/certs.yml` which has the following content

```yaml
- name: Include certs tasks
  include_role:
    name: c2platform.core.cacerts2
    tasks_from: certs
  vars:
    cacerts2_role_name: apache
```

With the `c2platform.core.cacerts2` role integrated we can now configure a list
`apache_cacerts2_certificates`:


```yaml
apache_cacerts2_certificates:
  - common_name: "{{ hosts_domain|replace('.','_') }}"
    subject_alt_name:
    - "DNS:{{ hosts_domain }}"
    - "DNS:*.{{ hosts_domain }}"
    - "DNS:{{ ansible_hostname }}"
    - "DNS:{{ ansible_fqdn }}"
    - "IP:{{ ansible_eth1.ipv4.address }}"
    ansible_group: reverse_proxy
    deploy:
      key:
        dir: /etc/ssl/private
        owner: www-data
        group: www-data
        mode: '640'
      crt:
        dir: /etc/ssl/certs
        owner: www-data
        group: www-data
        mode: '644'
```

The plugin module [set_certificate_facts](../plugins/modules/set_certificate_facts) will enhance `apache_cacerts2_certificates` to for example something like


```yaml
apache_cacerts2_certificates:
-   common_name: seetoo_tech
    create:
        crt: /vagrant/.ca/c2d/apache/seetoo_tech-c2d-rproxy.crt
        csr: /vagrant/.ca/c2d/apache/seetoo_tech-c2d-rproxy.csr
        key: /vagrant/.ca/c2d/apache/seetoo_tech-c2d-rproxy.key
        p12: /vagrant/.ca/c2d/apache/seetoo_tech-c2d-rproxy.p12
        pem: /vagrant/.ca/c2d/apache/seetoo_tech-c2d-rproxy.pem
    deploy:
        crt:
            dest: /etc/ssl/certs/seetoo_tech-c2d-rproxy.crt
            dir: /etc/ssl/certs
            group: www-data
            mode: '644'
            owner: www-data
        key:
            dest: /etc/ssl/private/seetoo_tech-c2d-rproxy.key
            dir: /etc/ssl/private
            group: www-data
            mode: '640'
            owner: www-data
    dir: /vagrant/.ca/c2d/apache
    subject_alt_name:
    - DNS:seetoo.tech
    - DNS:*.seetoo.tech
    - DNS:c2d-rproxy
    - IP:1.1.1.3
```

On apache nodes the `deploy` config will create:

* Certificate `/etc/ssl/certs/seetoo_tech-c2d-rproxy.crt`
* Key `/etc/ssl/certs/seetoo_tech-c2d-rproxy.key`

On the CA Server this will create for example

```
.ca/
└── c2d
    ├── apache
    │   ├── keycloak-c2d-rproxy.crt
    │   ├── keycloak-c2d-rproxy.csr
    │   ├── keycloak-c2d-rproxy.key
    │   ├── keycloak-c2d-rproxy.p12
    │   ├── keycloak-c2d-rproxy.pem
    │   ├── seetoo_tech-c2d-rproxy.crt
    │   ├── seetoo_tech-c2d-rproxy.csr
    │   ├── seetoo_tech-c2d-rproxy.key
    │   ├── seetoo_tech-c2d-rproxy.p12
    │   └── seetoo_tech-c2d-rproxy.pem
    ├── c2d.crt
    ├── c2d.csr
    └── c2d.key
```

#### `harbor_cacerts2_certificates`: Settings for Harbor certificates, including CA deployment.

It is also possible to "deploy" CA files see Harbor example below where we use
`deploy_ca` dictionary to deploy `ca.cert` on Harbor node.

```yaml
harbor_cacerts2_certificates:
  - common_name: registry
    subject_alt_name:
    - "DNS:{{ harbor_hostname }}"
    - "DNS:{{ ansible_hostname }}"
    - "DNS:{{ ansible_fqdn }}"
    - "IP:{{ ansible_eth1.ipv4.address }}"
    ansible_group: haproxy
    deploy:
      key:
        dir: "/etc/docker/certs.d/{{ harbor_hostname }}"
        dest: "/etc/docker/certs.d/{{ harbor_hostname }}/{{ harbor_hostname }}.key"
        mode: '640'
      crt:
        dir: "/etc/docker/certs.d/{{ harbor_hostname }}"
        dest: "/etc/docker/certs.d/{{ harbor_hostname }}/{{ harbor_hostname }}.cert"
        mode: '644'
    deploy_ca:
      crt:
        dir: "/etc/docker/certs.d/{{ harbor_hostname }}"
        dest: "/etc/docker/certs.d/{{ harbor_hostname }}/ca.crt"
        mode: '644'
```

#### `tomcat_cacerts2_certificates`: Details for distributing certificates to Java KeyStores and managing Java TrustStores.

This Ansible role extends its capabilities to distribute generated certificates
into Java KeyStores. And it can manage Java TrustStores. This is documented in
detail on the community website see
[Tomcat SSL/TLS and Java Keystore and Trustore Configuration for Linux and Windows Hosts](https://c2platform.org/en/docs/howto/rws/certs/)


## Dependencies

<!--   A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles. -->

## Example Play

<!--   Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too: -->

For a comprehensive example that demonstrates the use of this role in a fully configured setup, refer to the
[Tomcat SSL/TLS and Java Keystore and Trustore Configuration for Linux and Windows Hosts](https://c2platform.org/en/docs/howto/rws/certs/)

## Additional Resources

* [Designing a Public Key Infrastructure (PKI) for RWS GIS with Ansible](https://c2platform.org/en/docs/projects/rws/pki)
* [Tomcat SSL/TLS and Java Keystore and Trustore Configuration for Linux and Windows Hosts](https://c2platform.org/en/docs/howto/rws/certs/)
* [How to create a small CA — Ansible Documentation](https://docs.ansible.com/ansible/latest/collections/community/crypto/docsite/guide_ownca.html)

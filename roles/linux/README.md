# Ansible Role c2platform.core.linux

This Ansible role, `c2platform.core.linux`, is designed for the efficient
management of Linux systems by leveraging +112 modules from the the
`ansible.builtin`, `ansible.posix`, and `community.general` collections. Its
primary objective is to streamline the administration of common Linux resources.
By incorporating this role, you eliminate the need for custom development of
Ansible collections or roles. Configuration is simplified through the use of a
universal variable `linux_resources` or more specific, yet still versatile,
module variables such as `linux_replace`, `linux_copy` etc.

- [Requirements](#requirements)
- [Role Variables](#role-variables)
  - [`linux_roles`](#linux_roles)
  - [`linux_<module>` and `linux_resources_types`](#linux_module-and-linux_resources_types)
  - [`linux_resources`](#linux_resources)
  - [Understanding `type` vs `module` in `linux_resources`](#understanding-type-vs-module-in-linux_resources)
  - [Leveraging `resources` and `defaults` for Efficiency](#leveraging-resources-and-defaults-for-efficiency)
  - [Using Dictionaries and Lists](#using-dictionaries-and-lists)
  - [Modules](#modules)
    - [`acl`: Set and retrieve file ACL information.](#acl-set-and-retrieve-file-acl-information)
    - [`alternatives`: Manages alternative programs for common commands](#alternatives-manages-alternative-programs-for-common-commands)
    - [`apk`: Manages apk packages](#apk-manages-apk-packages)
    - [`apt`: Manages apt-packages](#apt-manages-apt-packages)
    - [`apt_key`: Add or remove an apt key](#apt_key-add-or-remove-an-apt-key)
    - [`apt_repo`: Manage APT repositories via apt-repo](#apt_repo-manage-apt-repositories-via-apt-repo)
    - [`apt_repository`: Add and remove APT repositories](#apt_repository-add-and-remove-apt-repositories)
    - [`apt_rpm`: APT-RPM package manager](#apt_rpm-apt-rpm-package-manager)
    - [`archive`: Creates a compressed archive of one or more files or trees](#archive-creates-a-compressed-archive-of-one-or-more-files-or-trees)
    - [`at`: Schedule the execution of a command or script file via the at command](#at-schedule-the-execution-of-a-command-or-script-file-via-the-at-command)
    - [`authorized_key`: Adds or removes an SSH authorized key](#authorized_key-adds-or-removes-an-ssh-authorized-key)
    - [`blockinfile`: Insert/update/remove a text block surrounded by marker lines](#blockinfile-insertupdateremove-a-text-block-surrounded-by-marker-lines)
    - [`bundler`: Manage Ruby Gem dependencies with Bundler](#bundler-manage-ruby-gem-dependencies-with-bundler)
    - [`capabilities`: Manage Linux capabilities](#capabilities-manage-linux-capabilities)
    - [`command`: Execute commands on targets](#command-execute-commands-on-targets)
    - [`copy`: Copy files to remote locations](#copy-copy-files-to-remote-locations)
    - [`cpanm`: Manages Perl library dependencies](#cpanm-manages-perl-library-dependencies)
    - [`cron`: Manage cron.d and crontab entries](#cron-manage-crond-and-crontab-entries)
    - [`cronvar`: Manage variables in crontabs](#cronvar-manage-variables-in-crontabs)
    - [`crypttab`: Encrypted Linux block devices](#crypttab-encrypted-linux-block-devices)
    - [`dconf`: Modify and read dconf database](#dconf-modify-and-read-dconf-database)
    - [`deb822_repository`: Add and remove deb822 formatted repositories](#deb822_repository-add-and-remove-deb822-formatted-repositories)
    - [`debconf`: Configure a .deb package](#debconf-configure-a-deb-package)
    - [`dnf`: Manages packages with the I(dnf) package manager](#dnf-manages-packages-with-the-idnf-package-manager)
    - [`dnf5`: Manages packages with the I(dnf5) package manager](#dnf5-manages-packages-with-the-idnf5-package-manager)
    - [`dnf_versionlock`: Locks package versions in C(dnf) based systems](#dnf_versionlock-locks-package-versions-in-cdnf-based-systems)
    - [`dpkg_divert`: Override a debian package's version of a file](#dpkg_divert-override-a-debian-packages-version-of-a-file)
    - [`dpkg_selections`: Dpkg package selection selections](#dpkg_selections-dpkg-package-selection-selections)
    - [`easy_install`: Installs Python libraries](#easy_install-installs-python-libraries)
    - [`fail`: Fail with custom message](#fail-fail-with-custom-message)
    - [`fetch`: Fetch files from remote nodes](#fetch-fetch-files-from-remote-nodes)
    - [`file`: Manage files and file properties](#file-manage-files-and-file-properties)
    - [`filesize`: Create a file with a given size, or resize it if it exists](#filesize-create-a-file-with-a-given-size-or-resize-it-if-it-exists)
    - [`filesystem`: Makes a filesystem](#filesystem-makes-a-filesystem)
    - [`firewalld`: Manage arbitrary ports/services with firewalld](#firewalld-manage-arbitrary-portsservices-with-firewalld)
    - [`flatpak`: Manage flatpaks](#flatpak-manage-flatpaks)
    - [`flatpak_remote`: Manage flatpak repository remotes](#flatpak_remote-manage-flatpak-repository-remotes)
    - [`gconftool2`: Edit GNOME Configurations](#gconftool2-edit-gnome-configurations)
    - [`gconftool2_info`: Retrieve GConf configurations](#gconftool2_info-retrieve-gconf-configurations)
    - [`gem`: Manage Ruby gems](#gem-manage-ruby-gems)
    - [`get_url`: Downloads files from HTTP, HTTPS, or FTP to node](#get_url-downloads-files-from-http-https-or-ftp-to-node)
    - [`git`: Deploy software (or files) from git checkouts](#git-deploy-software-or-files-from-git-checkouts)
    - [`git_config`: Read and write git configuration](#git_config-read-and-write-git-configuration)
    - [`haproxy`: Enable, disable, and set weights for HAProxy backend servers using socket commands](#haproxy-enable-disable-and-set-weights-for-haproxy-backend-servers-using-socket-commands)
    - [`hostname`: Manage hostname](#hostname-manage-hostname)
    - [`htpasswd`: Manage user files for basic authentication](#htpasswd-manage-user-files-for-basic-authentication)
    - [`include_role`: Load and execute a role](#include_role-load-and-execute-a-role)
    - [`iptables`: Modify iptables rules](#iptables-modify-iptables-rules)
    - [`java_cert`: Uses keytool to import/remove certificate to/from java keystore (cacerts)](#java_cert-uses-keytool-to-importremove-certificate-tofrom-java-keystore-cacerts)
    - [`java_keystore`: Create a Java keystore in JKS format](#java_keystore-create-a-java-keystore-in-jks-format)
    - [`known_hosts`: Add or remove a host from the C(known\_hosts) file](#known_hosts-add-or-remove-a-host-from-the-cknown_hosts-file)
    - [`lineinfile`: Manage lines in text files](#lineinfile-manage-lines-in-text-files)
    - [`locale_gen`: Creates or removes locales](#locale_gen-creates-or-removes-locales)
    - [`lvg`: Configure LVM volume groups](#lvg-configure-lvm-volume-groups)
    - [`lvg_rename`: Renames LVM volume groups](#lvg_rename-renames-lvm-volume-groups)
    - [`lvol`: Configure LVM logical volumes](#lvol-configure-lvm-logical-volumes)
    - [`lxc_container`: Manage LXC Containers](#lxc_container-manage-lxc-containers)
    - [`lxd_container`: Manage LXD instances](#lxd_container-manage-lxd-instances)
    - [`lxd_profile`: Manage LXD profiles](#lxd_profile-manage-lxd-profiles)
    - [`lxd_project`: Manage LXD projects](#lxd_project-manage-lxd-projects)
    - [`mail`: Send an email](#mail-send-an-email)
    - [`mount`: Control active and configured mount points](#mount-control-active-and-configured-mount-points)
    - [`nmcli`: Manage Networking](#nmcli-manage-networking)
    - [`npm`: Manage node.js packages with npm](#npm-manage-nodejs-packages-with-npm)
    - [`package`: Generic OS package manager](#package-generic-os-package-manager)
    - [`pacman`: Manage packages with I(pacman)](#pacman-manage-packages-with-ipacman)
    - [`pacman_key`: Manage pacman's list of trusted keys](#pacman_key-manage-pacmans-list-of-trusted-keys)
    - [`pam_limits`: Modify Linux PAM limits](#pam_limits-modify-linux-pam-limits)
    - [`pamd`: Manage PAM Modules](#pamd-manage-pam-modules)
    - [`ping`: Verify connection / Python](#ping-verify-connection--python)
    - [`parted`: Configure block device partitions](#parted-configure-block-device-partitions)
    - [`patch`: Apply patch files using the GNU patch tool](#patch-apply-patch-files-using-the-gnu-patch-tool)
    - [`pause`: Pause playbook execution](#pause-pause-playbook-execution)
    - [`pip`: Manages Python library dependencies](#pip-manages-python-library-dependencies)
    - [`reboot`: Reboot a machine](#reboot-reboot-a-machine)
    - [`replace`: Replace all instances of a particular string in a file using a back-referenced regular expression](#replace-replace-all-instances-of-a-particular-string-in-a-file-using-a-back-referenced-regular-expression)
    - [`rhsm_release`: Set or Unset RHSM Release version](#rhsm_release-set-or-unset-rhsm-release-version)
    - [`rhsm_repository`: Manage RHSM repositories using the subscription-manager command](#rhsm_repository-manage-rhsm-repositories-using-the-subscription-manager-command)
    - [`rpm_key`: Adds or removes a gpg key from the rpm db](#rpm_key-adds-or-removes-a-gpg-key-from-the-rpm-db)
    - [`runit`: Manage runit services](#runit-manage-runit-services)
    - [`script`: Runs a local script on a remote node after transferring it](#script-runs-a-local-script-on-a-remote-node-after-transferring-it)
    - [`seboolean`: Toggles SELinux booleans](#seboolean-toggles-selinux-booleans)
    - [`sefcontext`: Manages SELinux file context mapping definitions](#sefcontext-manages-selinux-file-context-mapping-definitions)
    - [`selinux`: Change policy and state of SELinux](#selinux-change-policy-and-state-of-selinux)
    - [`selinux_permissive`: Change permissive domain in SELinux policy](#selinux_permissive-change-permissive-domain-in-selinux-policy)
    - [`selogin`: Manages linux user to SELinux user mapping](#selogin-manages-linux-user-to-selinux-user-mapping)
    - [`service`: Manage services](#service-manage-services)
    - [`shell`: Execute shell commands on targets](#shell-execute-shell-commands-on-targets)
    - [`shutdown`: Shut down a machine](#shutdown-shut-down-a-machine)
    - [`slurp`: Slurps a file from remote nodes](#slurp-slurps-a-file-from-remote-nodes)
    - [`snap`: Manages snaps](#snap-manages-snaps)
    - [`snap_alias`: Manages snap aliases](#snap_alias-manages-snap-aliases)
    - [`ssh_config`: Manage SSH config for user](#ssh_config-manage-ssh-config-for-user)
    - [`sudoers`: Manage sudoers files](#sudoers-manage-sudoers-files)
    - [`synchronize`: A wrapper around rsync to make common tasks in your playbooks quick and easy](#synchronize-a-wrapper-around-rsync-to-make-common-tasks-in-your-playbooks-quick-and-easy)
    - [`sysctl`: Manage entries in sysctl.conf.](#sysctl-manage-entries-in-sysctlconf)
    - [`syslogger`: Log messages in the syslog](#syslogger-log-messages-in-the-syslog)
    - [`systemd`: Manage systemd units](#systemd-manage-systemd-units)
    - [`systemd_service`: Manage systemd units](#systemd_service-manage-systemd-units)
    - [`sysvinit`: Manage SysV services.](#sysvinit-manage-sysv-services)
    - [`tempfile`: Creates temporary files and directories](#tempfile-creates-temporary-files-and-directories)
    - [`template`: Template a file out to a target host](#template-template-a-file-out-to-a-target-host)
    - [`timezone`: Configure timezone setting](#timezone-configure-timezone-setting)
    - [`ufw`: Manage firewall with UFW](#ufw-manage-firewall-with-ufw)
    - [`unarchive`: Unpacks an archive after (optionally) copying it from the local machine](#unarchive-unpacks-an-archive-after-optionally-copying-it-from-the-local-machine)
    - [`uri`: Interacts with webservices](#uri-interacts-with-webservices)
    - [`user`: Manage user accounts](#user-manage-user-accounts)
    - [`wait_for`: Waits for a condition before continuing](#wait_for-waits-for-a-condition-before-continuing)
    - [`wait_for_connection`: Waits until remote system is reachable/usable](#wait_for_connection-waits-until-remote-system-is-reachableusable)
    - [`xml`: Manage bits and pieces of XML files or strings](#xml-manage-bits-and-pieces-of-xml-files-or-strings)
    - [`yum`: Manages packages with the I(yum) package manager](#yum-manages-packages-with-the-iyum-package-manager)
    - [`yum_repository`: Add or remove YUM repositories](#yum_repository-add-or-remove-yum-repositories)
    - [`yum_versionlock`: Locks / unlocks a installed package(s) from being updated by yum package manager](#yum_versionlock-locks--unlocks-a-installed-packages-from-being-updated-by-yum-package-manager)
    - [`zfs`: Manage zfs](#zfs-manage-zfs)
    - [`zfs_delegate_admin`: Manage ZFS delegated administration (user admin privileges)](#zfs_delegate_admin-manage-zfs-delegated-administration-user-admin-privileges)
    - [`zfs_facts`: Gather facts about ZFS datasets](#zfs_facts-gather-facts-about-zfs-datasets)
- [Handlers](#handlers)
  - [Handlers List](#handlers-list)
    - [`firewalld-reloaded`: Reloads firewalld](#firewalld-reloaded-reloads-firewalld)
    - [`systemctl-daemon-reload`: Run the equivalent of systemctl daemon-reload](#systemctl-daemon-reload-run-the-equivalent-of-systemctl-daemon-reload)
  - [Usage Examples](#usage-examples)
- [Dependencies](#dependencies)
- [Example Playbook](#example-playbook)

## Requirements

<!-- Any pre-requisites that may not be covered by Ansible itself or the role should be mentioned here. For instance, if the role uses the EC2 module, it may be a good idea to mention in this section that the boto package is required. -->

## Role Variables

<!--  A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well. -->

### `linux_roles`

Use the list `linux_roles` to configure Ansible roles form this collection that the provisioning of this role will be dependent on
for example:

```yaml
linux_roles`:
  - secrets
```

The roles you can include are:

* `server_update`
* `apt_repo`
* `yum`
* `ootstrap`
* `os_trusts`
* `secrets`
* `files`
* `lcm`
* `lvm`
* `hosts`
* `alias`

see [meta/main.yml](./meta/main.yml)

### `linux_<module>` and `linux_resources_types`

To harness the power of the 117+ modules supported by this role, employ the
`linux_<module>` variable alongside the `linux_resource_types` variable. These
variables are identical in scope and function to their `win_<module>` and
`win_resources_types` counterparts, so refer to the `c2platform.core.win`
Ansible role for documentation on how they are used.

### `linux_resources`

The `linux_resources` variable offers greater control and flexibility compared
to `linux_<module>` by allowing precise management over task execution order and
module reuse at various stages, thus facilitating dependency management. This
variable is identical in scope and function to the `win_resources` variable, so
refer to the `c2platform.core.win` Ansible role for documentation on how it is
used.

### Understanding `type` vs `module` in `linux_resources`

In the context of the `linux_resources` variable, the term `module` serves as an
alias for `type`. This aliasing can be particularly useful when working with
Ansible modules that already include a `type` parameter. For more information
about this, refer to the `c2platform.core.win` Ansible role documentation.

### Leveraging `resources` and `defaults` for Efficiency

The `linux_resources` and `linux_<module>` variables support the `resources` and
`defaults` keys to streamline your code, adhering to the **DRY** (Don't Repeat
Yourself) and **DIE** (Duplication is Evil) principles in software development.
These principles advocate for reducing repetition to enhance maintainability and
efficiency. For more information about these principles and how to apply them in
your code, refer to the `c2platform.core.win` Ansible role documentation.

### Using Dictionaries and Lists

In this section, we discuss the flexibility of using dictionaries or lists to
define variables in this Ansible role. The choice between the two depends on
your needs, and utilizing a dictionary can offer a significant advantage when
dealing with the configuration of different server groups. See the guideline
[Managing Dictionary
Merging](http://c2platform.org/docs/guidelines/coding/merge/) for more
information about this advanced Ansible feature.

Also refer to the `c2platform.core.win` Ansible role for a practical example
demonstrating these principles in action.

### Modules

<!-- start supported_modules -->
This roles supports a selection of 115 modules
from the collections `ansible.builtin`, `community.general`, `ansible.posix`, `community.crypto`. This section
contains simple examples on how these modules can be used via the `linux_resources` and
`linux_<module_name>` variables.


#### `acl`: Set and retrieve file ACL information.

```yaml
linux_acl:
  - path: /etc/foo.conf
    entity: joe
    etype: user
    permissions: r
    state: present

```

```yaml
linux_resources:
  - module: acl
    path: /etc/foo.d/
    entity: joe
    etype: user
    permissions: rw
    default: true
    state: present

```

For more examples and information on options see
[`ansible.posix.acl`](https://docs.ansible.com/ansible/latest/collections/ansible/posix/acl_module.html).
#### `alternatives`: Manages alternative programs for common commands

```yaml
linux_alternatives:
  - name: java
    path: /usr/lib/jvm/java-7-openjdk-amd64/jre/bin/java

```

```yaml
linux_resources:
  - module: alternatives
    link: /etc/hadoop/conf
    name: hadoop-conf
    path: /etc/hadoop/conf.ansible
    state: selected

```

For more examples and information on options see
[`community.general.alternatives`](https://docs.ansible.com/ansible/latest/collections/community/general/alternatives_module.html).
#### `apk`: Manages apk packages

```yaml
linux_apk:
  - name: foo
    state: latest
    update_cache: true

```

```yaml
linux_resources:
  - module: apk
    name: bar
    state: absent

```

For more examples and information on options see
[`community.general.apk`](https://docs.ansible.com/ansible/latest/collections/community/general/apk_module.html).
#### `apt`: Manages apt-packages

```yaml
linux_apt:
  - name: apache2
    state: present

```

```yaml
linux_resources:
  - module: apt
    name: zfsutils-linux
    state: latest
    fail_on_autoremove: yes

```

For more examples and information on options see
[`ansible.builtin.apt`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/apt_module.html).
#### `apt_key`: Add or remove an apt key

```yaml
linux_apt_key:
  - id: 36A1D7869245C8950F966E92D8576A8BA88D21E9
    keyserver: keyserver.ubuntu.com
  - url: https://ftp-master.debian.org/keys/archive-key-6.0.asc
    state: present

```

```yaml
linux_resources:
  - module: apt_key
    id: 9FED2BCBDCD29CDF762678CBAED4B06F473041FA
    url: https://ftp-master.debian.org/keys/archive-key-6.0.asc
    state: present

```

For more examples and information on options see
[`ansible.builtin.apt_key`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/apt_key_module.html).
#### `apt_repo`: Manage APT repositories via apt-repo

```yaml
linux_apt_repo:
  - repo: all
    state: absent

```

```yaml
linux_resources:
  - module: apt_repo
    repo: Sisysphus
    state: present
    remove_others: true

```

For more examples and information on options see
[`community.general.apt_repo`](https://docs.ansible.com/ansible/latest/collections/community/general/apt_repo_module.html).
#### `apt_repository`: Add and remove APT repositories

```yaml
linux_apt_repository:
  - repo: deb http://archive.canonical.com/ubuntu hardy partner
    state: present

```

```yaml
linux_resources:
  - module: apt_repository
    repo: deb http://dl.google.com/linux/chrome/deb/ stable main
    state: present

```

For more examples and information on options see
[`ansible.builtin.apt_repository`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/apt_repository_module.html).
#### `apt_rpm`: APT-RPM package manager

```yaml
linux_apt_rpm:
  - pkg: foo
    state: present

```

```yaml
linux_resources:
  - module: apt_rpm
    package: bar
    state: present

```

For more examples and information on options see
[`community.general.apt_rpm`](https://docs.ansible.com/ansible/latest/collections/community/general/apt_rpm_module.html).
#### `archive`: Creates a compressed archive of one or more files or trees

```yaml
linux_archive:
  - path: /path/to/foo
    dest: /path/to/foo.tgz

```

```yaml
linux_resources:
  - module: archive
    path: /path/to/foo
    dest: /path/to/foo.tgz

```

For more examples and information on options see
[`community.general.archive`](https://docs.ansible.com/ansible/latest/collections/community/general/archive_module.html).
#### `at`: Schedule the execution of a command or script file via the at command

```yaml
linux_at:
  - command: ls -d / >/dev/null
    count: 20
    units: minutes

```

```yaml
linux_resources:
  - module: at
    command: ls -d /etc >/dev/null
    count: 10
    units: hours

```

For more examples and information on options see
[`ansible.posix.at`](https://docs.ansible.com/ansible/latest/collections/ansible/posix/at_module.html).
#### `authorized_key`: Adds or removes an SSH authorized key

```yaml
linux_authorized_key:
  - user: charlie
    state: present
    key: "{{ lookup('file', '/home/charlie/.ssh/id_rsa.pub') }}"
  - user: charlie
    state: present
    key: https://github.com/charlie.keys

```

```yaml
linux_resources:
  - module: authorized_key
    user: deploy
    state: present
    key: '{{ item }}'
  - module: authorized_key
    user: root
    key: "{{ lookup('file', 'public_keys/doe-jane') }}"
    state: present
    exclusive: true

```

For more examples and information on options see
[`ansible.posix.authorized_key`](https://docs.ansible.com/ansible/latest/collections/ansible/posix/authorized_key_module.html).
#### `blockinfile`: Insert/update/remove a text block surrounded by marker lines

```yaml
linux_blockinfile:
  - path: /etc/ssh/sshd_config
    block: |
      Match User ansible-agent
      PasswordAuthentication no
  - path: /etc/network/interfaces
    block: |
      iface eth0 inet static
          address 192.0.2.23
          netmask 255.255.255.0

```

```yaml
linux_resources:
  - module: blockinfile
    backup: yes
    block: "{{ lookup('ansible.builtin.file', './local/sshd_config') }}"
    path: /etc/ssh/sshd_config
    validate: /usr/sbin/sshd -T -f %s

```

For more examples and information on options see
[`ansible.builtin.blockinfile`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/blockinfile_module.html).
#### `bundler`: Manage Ruby Gem dependencies with Bundler

```yaml
linux_bundler:
  - state: present
    executable: ~/.rvm/gems/2.1.5/bin/bundle

```

```yaml
linux_resources:
  - module: bundler
    chdir: /path/to/directory
    state: present

```

For more examples and information on options see
[`community.general.bundler`](https://docs.ansible.com/ansible/latest/collections/community/general/bundler_module.html).
#### `capabilities`: Manage Linux capabilities

```yaml
linux_capabilities:
  - path: /foo
    capability: cap_sys_chroot+ep
    state: present
  - path: /bar
    capability: cap_net_bind_service
    state: absent

```

```yaml
linux_resources:
  - module: capabilities
    capability: cap_sys_admin+ep
    path: /bin/chmod
    state: present

```

For more examples and information on options see
[`community.general.capabilities`](https://docs.ansible.com/ansible/latest/collections/community/general/capabilities_module.html).
#### `command`: Execute commands on targets

```yaml
linux_command:
    - cmd: cat /etc/motd
linux_resources:
    - module: command
      cmd: /usr/bin/make_database.sh db_user db_name creates=/path/to/database

```

For more examples and information on options see
[`ansible.builtin.command`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/command_module.html).
#### `copy`: Copy files to remote locations

```yaml
linux_copy:
  - src: /srv/myfiles/foo.conf
    dest: /etc/foo.conf
    owner: foo
    group: foo
    mode: '0644'

```

```yaml
linux_resources:
  - module: copy
    src: /srv/myfiles/foo.conf
    dest: /etc/foo.conf
    owner: foo
    group: foo
    mode: '0644'

```

For more examples and information on options see
[`ansible.builtin.copy`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/copy_module.html).
#### `cpanm`: Manages Perl library dependencies

```yaml
linux_cpanm:
  - name: Dancer
  - name: MIYAGAWA/Plack-0.99_05.tar.gz
  - name: Dancer
    locallib: /srv/webapps/my_app/extlib

```

```yaml
linux_resources:
  - module: cpanm
    executable: /usr/local/bin/cpanm
    from_path: /tmp/cpan_modules
  - module: cpanm
    notest: true
    name: Dancer

```

For more examples and information on options see
[`community.general.cpanm`](https://docs.ansible.com/ansible/latest/collections/community/general/cpanm_module.html).
#### `cron`: Manage cron.d and crontab entries

```yaml
linux_cron:
  - name: "check dirs"
    minute: "0"
    hour: "5,2"
    job: "ls -alh > /dev/null"

```

```yaml
linux_resources:
  - module: cron
    name: "yum autoupdate"
    weekday: "2"
    minute: "0"
    hour: "12"
    user: "root"
    job: "YUMINTERACTIVE=0 /usr/sbin/yum-autoupdate"
    cron_file: "ansible_yum-autoupdate"

```

For more examples and information on options see
[`ansible.builtin.cron`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/cron_module.html).
#### `cronvar`: Manage variables in crontabs

```yaml
linux_cronvar:
    - name: EMAIL
      value: doug@ansibmod.con.com

```

```yaml
linux_resources:
    - module: cronvar
      name: LOGFILE
      value: /var/log/yum-autoupdate.log
      user: root
      cron_file: ansible_yum-autoupdate

```

For more examples and information on options see
[`community.general.cronvar`](https://docs.ansible.com/ansible/latest/collections/community/general/cronvar_module.html).
#### `crypttab`: Encrypted Linux block devices

```yaml
linux_crypttab:
    - name: luks-home
      state: present
      opts: discard,cipher=aes-cbc-essiv:sha256

```

```yaml
linux_resources:
    - module: crypttab
      name: luks-data
      state: present
      opts: discard

```

For more examples and information on options see
[`community.general.crypttab`](https://docs.ansible.com/ansible/latest/collections/community/general/crypttab_module.html).
#### `dconf`: Modify and read dconf database

```yaml
linux_dconf:
  - key: "/org/gnome/desktop/input-sources/sources"
    value: "[('xkb', 'us'), ('xkb', 'se')]"
    state: present

```

```yaml
linux_resources:
  - module: dconf
    key: "/org/gnome/desktop/input-sources/sources"
    value: "[('xkb', 'us'), ('xkb', 'se')]"
    state: present

```

For more examples and information on options see
[`community.general.dconf`](https://docs.ansible.com/ansible/latest/collections/community/general/dconf_module.html).
#### `deb822_repository`: Add and remove deb822 formatted repositories

```yaml
linux_deb822_repository:
  - name: debian
    types: deb
    uris: http://deb.debian.org/debian
    suites:
      - stretch
    components:
      - main
      - contrib
      - non-free

```

```yaml
linux_resources:
  - module: deb822_repository
    name: centos
    types: deb
    uris: http://mirror.centos.org/centos/
    components:
      - base
      - updates
      - extras

```

For more examples and information on options see
[`ansible.builtin.deb822_repository`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/deb822_repository_module.html).
#### `debconf`: Configure a .deb package

```yaml
linux_debconf:
  - name: locales
    question: locales/default_environment_locale
    value: fr_FR.UTF-8
    vtype: select

```

```yaml
linux_resources:
  - module: debconf
    name: locales
    question: locales/locales_to_be_generated
    value: en_US.UTF-8 UTF-8, fr_FR.UTF-8 UTF-8
    vtype: multiselect

```

For more examples and information on options see
[`ansible.builtin.debconf`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/debconf_module.html).
#### `dnf`: Manages packages with the I(dnf) package manager

```yaml
linux_dnf:
  - name: httpd
    state: latest

```

```yaml
linux_resources:
  - module: dnf
    name: httpd
    state: latest

```

For more examples and information on options see
[`ansible.builtin.dnf`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/dnf_module.html).
#### `dnf5`: Manages packages with the I(dnf5) package manager

```yaml
linux_dnf5:
  - name: httpd
    state: latest

```

```yaml
linux_resources:
  - module: dnf5
    name: httpd
    state: latest

```

For more examples and information on options see
[`ansible.builtin.dnf5`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/dnf5_module.html).
#### `dnf_versionlock`: Locks package versions in C(dnf) based systems

```yaml
linux_dnf_versionlock:
  - name: nginx
    state: present

```

```yaml
linux_resources:
  - module: dnf_versionlock
    name: nginx
    state: present

```

For more examples and information on options see
[`community.general.dnf_versionlock`](https://docs.ansible.com/ansible/latest/collections/community/general/dnf_versionlock_module.html).
#### `dpkg_divert`: Override a debian package's version of a file

```yaml
linux_dpkg_divert:
  - path: /usr/bin/busybox
  - path: /usr/bin/busybox
    holder: branding
  - path: /usr/bin/busybox
    divert: /usr/bin/busybox.dpkg-divert
    rename: true
  - path: /usr/bin/busybox
    state: absent
    rename: true
    force: true

```

```yaml
linux_resources:
  - module: dpkg_divert
    divert: /usr/local/bin/example
    holder: example_package
    state: present

```

For more examples and information on options see
[`community.general.dpkg_divert`](https://docs.ansible.com/ansible/latest/collections/community/general/dpkg_divert_module.html).
#### `dpkg_selections`: Dpkg package selection selections

```yaml
linux_dpkg_selections:
    - name: python
      selection: hold
    - name: ansible
      selection: install

```

```yaml
linux_resources:
    - module: dpkg_selections
      name: vim
      selection: deinstall

```

For more examples and information on options see
[`ansible.builtin.dpkg_selections`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/dpkg_selections_module.html).
#### `easy_install`: Installs Python libraries

```yaml
linux_easy_install:
  - name: Flask
    state: latest
  - name: numpy
    state: latest

```

```yaml
linux_resources:
  - module: easy_install
    executable: my_custom_easy_install
    virtualenv: /path/to/venv

```

For more examples and information on options see
[`community.general.easy_install`](https://docs.ansible.com/ansible/latest/collections/community/general/easy_install_module.html).
#### `fail`: Fail with custom message

```yaml
linux_fail:
    - msg: The system may not be provisioned according to the CMDB status.

```

```yaml
linux_resources:
    - module: fail
      msg: Custom message for failure

```

For more examples and information on options see
[`ansible.builtin.fail`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/fail_module.html).
#### `fetch`: Fetch files from remote nodes

```yaml
linux_fetch:
    - src: /tmp/somefile
      dest: /tmp/fetched

```

```yaml
linux_resources:
    - module: fetch
      dest: /tmp/prefix-{{ inventory_hostname }}
      src: /tmp/somefile
      flat: yes

```

For more examples and information on options see
[`ansible.builtin.fetch`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/fetch_module.html).
#### `file`: Manage files and file properties

```yaml
linux_file:
  - path: /etc/foo.conf
    owner: foo
    group: foo
    mode: '0644'

```

```yaml
linux_resources:
  - module: file
    access_time: now
    access_time_format: '%Y%m%d%H%M.%S'

```

For more examples and information on options see
[`ansible.builtin.file`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/file_module.html).
#### `filesize`: Create a file with a given size, or resize it if it exists

```yaml
linux_filesize:
  - path: /var/bigfile
    size: 1G

```

```yaml
linux_resources:
  - module: filesize
    path: /var/bigfile
    size: 1G

```

For more examples and information on options see
[`community.general.filesize`](https://docs.ansible.com/ansible/latest/collections/community/general/filesize_module.html).
#### `filesystem`: Makes a filesystem

```yaml
linux_filesystem:
  - fstype: ext2
    dev: /dev/sdb1

```

```yaml
linux_resources:
  - module: filesystem
    dev: /dev/sdc
    fstype: ext4

```

For more examples and information on options see
[`community.general.filesystem`](https://docs.ansible.com/ansible/latest/collections/community/general/filesystem_module.html).
#### `firewalld`: Manage arbitrary ports/services with firewalld

```yaml
linux_firewalld:
  - service: https
    permanent: true
    state: enabled

```

```yaml
linux_resources:
  - module: firewalld
    port: 161-162/udp
    permanent: true
    state: enabled

```

For more examples and information on options see
[`ansible.posix.firewalld`](https://docs.ansible.com/ansible/latest/collections/ansible/posix/firewalld_module.html).
#### `flatpak`: Manage flatpaks

```yaml
linux_flatpak:
  - name: https://s3.amazonaws.com/alexlarsson/spotify-repo/spotify.flatpakref
    state: present

```

```yaml
linux_resources:
  - module: flatpak
    name:
      - org.gimp.GIMP
      - org.inkscape.Inkscape
      - org.mozilla.firefox
    state: present

```

For more examples and information on options see
[`community.general.flatpak`](https://docs.ansible.com/ansible/latest/collections/community/general/flatpak_module.html).
#### `flatpak_remote`: Manage flatpak repository remotes

```yaml
linux_flatpak_remote:
  - name: gnome
    state: present
    flatpakrepo_url: https://sdk.gnome.org/gnome-apps.flatpakrepo
  - name: flathub
    state: present
    flatpakrepo_url: https://dl.flathub.org/repo/flathub.flatpakrepo
    method: user
  - name: gnome
    state: absent
    method: user

```

```yaml
linux_resources:
  - module: flatpak_remote
    enabled: true
    executable: flatpak
    flatpakrepo_url: "{{ item['flatpakrepo_url'] | default(omit) }}"
    method: system
    name: "{{ item['name'] | default(omit) }}"
    state: "{{ item['state'] | default(omit) }}"

```

For more examples and information on options see
[`community.general.flatpak_remote`](https://docs.ansible.com/ansible/latest/collections/community/general/flatpak_remote_module.html).
#### `gconftool2`: Edit GNOME Configurations

```yaml
linux_gconftool2:
  - key: "/desktop/gnome/interface/font_name"
    value_type: "string"
    value: "Serif 12"

```

```yaml
linux_resources:
  - module: gconftool2
    key: "/desktop/gnome/interface/font_name"
    value_type: "string"
    value: "Serif 12"

```

For more examples and information on options see
[`community.general.gconftool2`](https://docs.ansible.com/ansible/latest/collections/community/general/gconftool2_module.html).
#### `gconftool2_info`: Retrieve GConf configurations

```yaml
linux_gconftool2_info:
  - key: /desktop/gnome/background/picture_filename
linux_resources:
  - module: gconftool2_info
    key: /desktop/gnome/background/picture_filename

```

For more examples and information on options see
[`community.general.gconftool2_info`](https://docs.ansible.com/ansible/latest/collections/community/general/gconftool2_info_module.html).
#### `gem`: Manage Ruby gems

```yaml
linux_gem:
  - name: vagrant
    version: 1.0
    state: present

```

```yaml
linux_resources:
  - module: gem
    name: rake
    state: latest

```

For more examples and information on options see
[`community.general.gem`](https://docs.ansible.com/ansible/latest/collections/community/general/gem_module.html).
#### `get_url`: Downloads files from HTTP, HTTPS, or FTP to node

```yaml
linux_get_url:
  - url: http://example.com/path/file.conf
    dest: /etc/foo.conf
    mode: '0440'

```

```yaml
linux_resources:
  - module: get_url
    force: true
    dest: /tmp/somefile.conf

```

For more examples and information on options see
[`ansible.builtin.get_url`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/get_url_module.html).
#### `git`: Deploy software (or files) from git checkouts

```yaml
linux_git:
  - repo: https://foosball.example.org/path/to/repo.git
    dest: /srv/checkout
    version: release-0.22

```

```yaml
linux_resources:
  - module: git
    dest: /path/to/checkouts
    repo: https://github.com/ansible/ansible-examples.git

```

For more examples and information on options see
[`ansible.builtin.git`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/git_module.html).
#### `git_config`: Read and write git configuration

```yaml
linux_git_config:
    - name: alias.ci
      scope: global
      value: commit
    - name: alias.st
      scope: global
      value: status

```

```yaml
linux_resources:
  - module: git_config
    name: alias.ci
    scope: global
    value: commit
  - module: git_config
    name: alias.st
    scope: global
    value: status

```

For more examples and information on options see
[`community.general.git_config`](https://docs.ansible.com/ansible/latest/collections/community/general/git_config_module.html).
#### `haproxy`: Enable, disable, and set weights for HAProxy backend servers using socket commands

```yaml
linux_haproxy:
  - state: disabled
    host: '{{ inventory_hostname }}'
    backend: www

```

```yaml
linux_resources:
  - module: haproxy
    agent: false
    backend: www

```

For more examples and information on options see
[`community.general.haproxy`](https://docs.ansible.com/ansible/latest/collections/community/general/haproxy_module.html).
#### `hostname`: Manage hostname

```yaml
linux_hostname:
    - name: web01
      use: systemd

```

```yaml
linux_resources:
    - module: hostname
      name: web02
      use: redhat

```

For more examples and information on options see
[`ansible.builtin.hostname`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/hostname_module.html).
#### `htpasswd`: Manage user files for basic authentication

```yaml
linux_htpasswd:
  - path: /etc/nginx/passwdfile
    name: janedoe
    password: '9s36?;fyNp'
    owner: root
    group: www-data
    mode: '0640'

```

```yaml
linux_resources:
  - module: htpasswd
    path: /etc/nginx/passwdfile
    name: janedoe
    password: '9s36?;fyNp'
    owner: root
    group: www-data
    mode: '0640'

```

For more examples and information on options see
[`community.general.htpasswd`](https://docs.ansible.com/ansible/latest/collections/community/general/htpasswd_module.html).
#### `include_role`: Load and execute a role

```yaml
linux_include_role:
  - name: secrets
  - name: c2platform.core.vagrant_hosts
```

```yaml
linux_resources:
  - name: secrets
    module: include_role
  - name: c2platform.core.vagrant_hosts
    module: include_role
```

For more examples and information on options see
[`ansible.builtin.include_role`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/include_role_module.html).
#### `iptables`: Modify iptables rules

```yaml
linux_iptables:
  - chain: INPUT
    source: 8.8.8.8
    jump: DROP
  - chain: PREROUTING
    in_interface: eth0
    protocol: tcp
    match: tcp
    destination_port: 80
    jump: REDIRECT
    to_ports: 8600
    comment: Redirect web traffic to port 8600

```

```yaml
linux_resources:
  - module: iptables
    chain: INPUT
    ctstate: ESTABLISHED,RELATED
    jump: ACCEPT

```

For more examples and information on options see
[`ansible.builtin.iptables`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/iptables_module.html).
#### `java_cert`: Uses keytool to import/remove certificate to/from java keystore (cacerts)

```yaml
linux_java_cert:
  - cert_url: google.com
    cert_port: 443
    keystore_path: /usr/lib/jvm/jre7/lib/security/cacerts
    keystore_pass: changeit
    state: present

```

```yaml
linux_resources:
  - module: java_cert
    cert_url: google.com
    keystore_path: /usr/lib/jvm/jre7/lib/security/cacerts
    keystore_pass: changeit
    state: absent

```

For more examples and information on options see
[`community.general.java_cert`](https://docs.ansible.com/ansible/latest/collections/community/general/java_cert_module.html).
#### `java_keystore`: Create a Java keystore in JKS format

```yaml
linux_java_keystore:
  - name: example
    certificate: |
      -----BEGIN CERTIFICATE-----
      h19dUZ2co2fI/ibYiwxWk4aeNE6KWvCaTQOMQ8t6Uo2XKhpL/xnjoAgh1uCQN/69
      MG+34+RhUWzCfdZH7T8/qDxJw2kEPKluaYh7KnMsba+5jHjmtzix5QIDAQABo4IB
      -----END CERTIFICATE-----
    private_key: |
      -----BEGIN RSA PRIVATE KEY-----
      DBVFTEVDVFJJQ0lURSBERSBGUFOQ0UxFzAVBgNVBAsMDjAwMDIgNTUyMDgxMzE3
      GLlDNMw/uHyME7gHFsqJA7O11VY6O5WQ4IDP3m/s5ZV6s+Nn6Lerz17VZ99
      -----END RSA PRIVATE KEY-----
    password: changeit
    dest: /etc/security/keystore.jks

```

```yaml
linux_resources:
  - module: java_keystore
    name: example
    certificate: |
      -----BEGIN CERTIFICATE-----
      h19dUZ2co2fI/ibYiwxWk4aeNE6KWvCaTQOMQ8t6Uo2XKhpL/xnjoAgh1uCQN/69
      MG+34+RhUWzCfdZH7T8/qDxJw2kEPKluaYh7KnMsba+5jHjmtzix5QIDAQABo4IB
      -----END CERTIFICATE-----
    private_key: |
      -----BEGIN RSA PRIVATE KEY-----
      DBVFTEVDVFJJQ0lURSBERSBGUFOQ0UxFzAVBgNVBAsMDjAwMDIgNTUyMDgxMzE3
      GLlDNMw/uHyME7gHFsqJA7O11VY6O5WQ4IDP3m/s5ZV6s+Nn6Lerz17VZ99
      -----END RSA PRIVATE KEY-----
    password: changeit
    dest: /etc/security/keystore.jks

```

For more examples and information on options see
[`community.general.java_keystore`](https://docs.ansible.com/ansible/latest/collections/community/general/java_keystore_module.html).
#### `known_hosts`: Add or remove a host from the C(known_hosts) file

```yaml
linux_known_hosts:
  - name: foo.com.invalid
    key: "{{ lookup('ansible.builtin.file', 'pubkeys/foo.com.invalid') }}"
    path: /etc/ssh/ssh_known_hosts
    state: present

```

```yaml
linux_resources:
  - module: known_hosts
    hash_host: no
    key: '[host1.example.com]:2222 ssh-rsa ASDeararAIUHI324324' # some key gibberish

```

For more examples and information on options see
[`ansible.builtin.known_hosts`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/known_hosts_module.html).
#### `lineinfile`: Manage lines in text files

```yaml
linux_lineinfile:
    - path: /etc/selinux/config
      regexp: '^SELINUX='
      line: SELINUX=enforcing
    - path: /etc/sudoers
      state: absent
      regexp: '^%wheel'

```

```yaml
linux_resources:
    - module: lineinfile
      path: /etc/hosts
      regexp: '^127\\.0\\.0\\.1'
      line: 127.0.0.1 localhost
      owner: root
      group: root
      mode: '0644'

```

For more examples and information on options see
[`ansible.builtin.lineinfile`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/lineinfile_module.html).
#### `locale_gen`: Creates or removes locales

```yaml
linux_locale_gen:
  - name: de_CH.UTF-8
    state: present
  - name: en_US.UTF-8
    state: present

```

```yaml
linux_resources:
  - module: locale_gen
    name: fr_FR.UTF-8
    state: present

```

For more examples and information on options see
[`community.general.locale_gen`](https://docs.ansible.com/ansible/latest/collections/community/general/locale_gen_module.html).
#### `lvg`: Configure LVM volume groups

```yaml
linux_lvg:
  - vg: vg.services
    pvs: /dev/sda1
    pesize: 32

```

```yaml
linux_resources:
  - module: lvg
    vg: resizableVG
    pvs: /dev/sda3
    pvresize: true

```

For more examples and information on options see
[`community.general.lvg`](https://docs.ansible.com/ansible/latest/collections/community/general/lvg_module.html).
#### `lvg_rename`: Renames LVM volume groups

```yaml
linux_lvg_rename:
  - vg: vg_orig_name
    vg_new: vg_new_name
  - vg_uuid: SNgd0Q-rPYa-dPB8-U1g6-4WZI-qHID-N7y9Vj
    vg_new: vg_new_name

```

```yaml
linux_resources:
  - module: lvg_rename
    vg: vg_name
    vg_new: vg_new_name

```

For more examples and information on options see
[`community.general.lvg_rename`](https://docs.ansible.com/ansible/latest/collections/community/general/lvg_rename_module.html).
#### `lvol`: Configure LVM logical volumes

```yaml
linux_lvol:
  - vg: firefly
    lv: test
    size: 512

```

```yaml
linux_resources:
  - module: lvol
    vg: firefly
    lv: test
    size: 512

```

For more examples and information on options see
[`community.general.lvol`](https://docs.ansible.com/ansible/latest/collections/community/general/lvol_module.html).
#### `lxc_container`: Manage LXC Containers

```yaml
linux_lxc_container:
  - name: test-container-started
    container_log: true
    template: ubuntu
    state: started
    template_options: --release trusty

```

```yaml
linux_resources:
  - module: lxc_container
    name: test-container-frozen
    container_log: true
    template: ubuntu
    state: frozen
    template_options: --release trusty

```

For more examples and information on options see
[`community.general.lxc_container`](https://docs.ansible.com/ansible/latest/collections/community/general/lxc_container_module.html).
#### `lxd_container`: Manage LXD instances

```yaml
linux_lxd_container:
  - name: mycontainer
    ignore_volatile_options: true
    state: started
    source:
      type: image
      mode: pull
      server: https://images.linuxcontainers.org
      protocol: lxd
      alias: ubuntu/xenial/amd64
    profiles: ["default"]
    wait_for_ipv4_addresses: true
    timeout: 600

```

```yaml
linux_resources:
  - module: lxd_container
    name: mycontainer2
    state: absent

```

For more examples and information on options see
[`community.general.lxd_container`](https://docs.ansible.com/ansible/latest/collections/community/general/lxd_container_module.html).
#### `lxd_profile`: Manage LXD profiles

```yaml
linux_lxd_profile:
  - name: macvlan
    state: present
    config: {}
    description: my macvlan profile
    devices:
      eth0:
        nictype: macvlan
        parent: br0
        type: nic

```

```yaml
linux_resources:
  - module: lxd_profile
    name: testprofile
    project: mytestproject
    state: present
    config: {}
    description: test profile in project mytestproject
    devices: {}

```

For more examples and information on options see
[`community.general.lxd_profile`](https://docs.ansible.com/ansible/latest/collections/community/general/lxd_profile_module.html).
#### `lxd_project`: Manage LXD projects

```yaml
linux_lxd_project:
    - name: ansible-test-project
      state: present
      config:
    - name: ansible-test-project-new-name
      new_name: ansible-test-project-renamed
      state: present
      config:
linux_resources:
    - module: lxd_project
      client_cert: /path/to/client.crt
      client_key: /path/to/client.key
      state: present
      url: unix:/var/lib/lxd/unix.socket
    - module: lxd_project
      name: ansible-test-project
      state: absent
      url: unix:/var/lib/lxd/unix.socket

```

For more examples and information on options see
[`community.general.lxd_project`](https://docs.ansible.com/ansible/latest/collections/community/general/lxd_project_module.html).
#### `mail`: Send an email

```yaml
linux_mail:
  - host: smtp.gmail.com
    port: 587
    username: username@gmail.com
    password: mysecret
    to:
      - John Smith <john.smith@example.com>
    subject: Ansible-report
    body: System {{ ansible_hostname }} has been successfully provisioned.

```

```yaml
linux_resources:
  - module: mail
    attach: "/etc/group"
    cc:
      - Charlie Root <root@localhost>
    headers:
      - "Reply-To=john@example.com"
    charset: us-ascii

```

For more examples and information on options see
[`community.general.mail`](https://docs.ansible.com/ansible/latest/collections/community/general/mail_module.html).
#### `mount`: Control active and configured mount points

```yaml
linux_mount:
  - path: /mnt/dvd
    src: /dev/sr0
    fstype: iso9660
    opts: ro,noauto
    state: present

```

```yaml
linux_resources:
  - module: mount
    backup: false
    boot: true

```

For more examples and information on options see
[`ansible.posix.mount`](https://docs.ansible.com/ansible/latest/collections/ansible/posix/mount_module.html).
#### `nmcli`: Manage Networking

```yaml
linux_nmcli:
  - conn_name: my-eth1
    ifname: eth1
    type: ethernet
    ip4: 192.0.2.100/24
    gw4: 192.0.2.1
    state: present
  - conn_name: my-team1
    ifname: my-team1
    type: team
    ip4: 192.0.2.100/24
    gw4: 192.0.2.1
    autoconnect: true
    state: present

```

```yaml
linux_resources:
  - module: nmcli
    conn_name: my-eth1
    ifname: eth1
    type: ethernet
    ip4: 192.0.2.100/24
    gw4: 192.0.2.1
    state: present
  - module: nmcli
    conn_name: my-team1
    ifname: my-team1
    type: team
    ip4: 192.0.2.100/24
    gw4: 192.0.2.1
    autoconnect: true
    state: present

```

For more examples and information on options see
[`community.general.nmcli`](https://docs.ansible.com/ansible/latest/collections/community/general/nmcli_module.html).
#### `npm`: Manage node.js packages with npm

```yaml
linux_npm:
  - name: coffee-script
    path: /app/location

```

```yaml
linux_resources:
  - module: npm
    name: coffee-script
    path: /app/location

```

For more examples and information on options see
[`community.general.npm`](https://docs.ansible.com/ansible/latest/collections/community/general/npm_module.html).
#### `package`: Generic OS package manager

```yaml
linux_package:
    - name: ntpdate
      state: present

```

```yaml
linux_resources:
    - module: package
      name: httpd
      state: latest
    - module: package
      name: mariadb-server
      state: latest

```

For more examples and information on options see
[`ansible.builtin.package`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/package_module.html).
#### `pacman`: Manage packages with I(pacman)

```yaml
linux_pacman:
  - name: foo
    state: present
  - name: ~/bar-1.0-1-any.pkg.tar.xz
    state: present

```

```yaml
linux_resources:
  - module: pacman
    name: foo
    state: present

```

For more examples and information on options see
[`community.general.pacman`](https://docs.ansible.com/ansible/latest/collections/community/general/pacman_module.html).
#### `pacman_key`: Manage pacman's list of trusted keys

```yaml
linux_pacman_key:
  - id: 01234567890ABCDE01234567890ABCDE12345678
    url: https://domain.tld/keys/keyfile.asc
    state: present

```

```yaml
linux_resources:
  - module: pacman_key
    id: 01234567890ABCDE01234567890ABCDE12345678
    url: https://domain.tld/keys/keyfile.asc
    state: present

```

For more examples and information on options see
[`community.general.pacman_key`](https://docs.ansible.com/ansible/latest/collections/community/general/pacman_key_module.html).
#### `pam_limits`: Modify Linux PAM limits

```yaml
linux_pam_limits:
  - domain: joe
    limit_type: soft
    limit_item: nofile
    value: 64000
  - domain: smith
    limit_type: hard
    limit_item: fsize
    value: 1000000
    use_max: true

```

```yaml
linux_resources:
  - module: pam_limits
    backup: true
    comment: "Setting limits"
    dest: "/etc/security/limits.d/custom.conf"
    domain: james
    limit_item: nofile
    limit_type: soft
    use_min: true
    value: 20000

```

For more examples and information on options see
[`community.general.pam_limits`](https://docs.ansible.com/ansible/latest/collections/community/general/pam_limits_module.html).
#### `pamd`: Manage PAM Modules

```yaml
linux_pamd:
    - name: system-auth
      type: auth
      control: required
      module_path: pam_faillock.so
      new_control: sufficient

```

```yaml
linux_resources:
    - module: pamd
      name: system-auth
      type: auth
      control: required
      module_path: pam_faillock.so
      new_control: sufficient

```

For more examples and information on options see
[`community.general.pamd`](https://docs.ansible.com/ansible/latest/collections/community/general/pamd_module.html).

#### `ping`: Verify connection / Python

```yaml
linux_ping:
  whatever:  # optional extra level for dictionary merging
    - data: "pong"

```

```yaml
win_resources:
  whatever:  # optional extra level for dictionary merging
    - module: linux_ping
      data: "pong"
```

For more examples and information on options see
[`ansible.builtin.ping`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/ping_module.html).


#### `parted`: Configure block device partitions

```yaml
linux_parted:
  - device: /dev/sdb
    number: 1
    state: present
    fs_type: ext4

```

```yaml
linux_resources:
  - module: parted
    device: /dev/sdc
    number: 2
    state: absent

```

For more examples and information on options see
[`community.general.parted`](https://docs.ansible.com/ansible/latest/collections/community/general/parted_module.html).
#### `patch`: Apply patch files using the GNU patch tool

```yaml
linux_patch:
  - src: /tmp/index.html.patch
    dest: /var/www/index.html

```

```yaml
linux_resources:
  - module: patch
    basedir: /var/www
    src: /tmp/customize.patch
    strip: 1

```

For more examples and information on options see
[`ansible.posix.patch`](https://docs.ansible.com/ansible/latest/collections/ansible/posix/patch_module.html).
#### `pause`: Pause playbook execution

```yaml
linux_pause:
  - minutes: 5

```

```yaml
linux_resources:
  - module: pause
    echo: yes
    prompt: "Enter a secret"

```

For more examples and information on options see
[`ansible.builtin.pause`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/pause_module.html).
#### `pip`: Manages Python library dependencies

```yaml
linux_pip:
  - name: bottle

```

```yaml
linux_resources:
  - module: pip
    name: bottle

```

For more examples and information on options see
[`ansible.builtin.pip`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/pip_module.html).
#### `reboot`: Reboot a machine

```yaml
linux_reboot:
    - post_reboot_delay: 30
      reboot_timeout: 300
    - reboot_command: systemctl reboot
      test_command: systemctl is-active sshd

```

```yaml
linux_resources:
    - module: reboot
      boot_time_command: uptime
      connect_timeout: 10
      msg: Rebooting the machine

```

For more examples and information on options see
[`ansible.builtin.reboot`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/reboot_module.html).
#### `replace`: Replace all instances of a particular string in a file using a back-referenced regular expression

```yaml
linux_replace:
  - path: /etc/hosts
    regexp: '(\\s+)old\\.host\\.name(\\s+.*)?$'
    replace: '\\1new.host.name\\2'

```

```yaml
linux_resources:
  - module: replace
    path: /etc/apache2/sites-available/default.conf
    after: 'NameVirtualHost [*]'
    regexp: '^(.+)$'
    replace: '# \\1'

```

For more examples and information on options see
[`ansible.builtin.replace`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/replace_module.html).
#### `rhsm_release`: Set or Unset RHSM Release version

```yaml
linux_rhsm_release:
    - release: "7.1"

```

```yaml
linux_resources:
    - module: rhsm_release
      release: "6Server"

```

For more examples and information on options see
[`community.general.rhsm_release`](https://docs.ansible.com/ansible/latest/collections/community/general/rhsm_release_module.html).
#### `rhsm_repository`: Manage RHSM repositories using the subscription-manager command

```yaml
linux_rhsm_repository:
  - name: rhel-7-server-rpms
  - name: rhel-8-server-rpms
    state: disabled

```

```yaml
linux_resources:
  - module: rhsm_repository
    name: rhel-6-beta-rpms
    purge: true
  - module: rhsm_repository
    name: rhel-7-server-rpms
    state: enabled

```

For more examples and information on options see
[`community.general.rhsm_repository`](https://docs.ansible.com/ansible/latest/collections/community/general/rhsm_repository_module.html).
#### `rpm_key`: Adds or removes a gpg key from the rpm db

```yaml
linux_rpm_key:
    - state: present
      key: http://apt.sw.be/RPM-GPG-KEY.dag.txt
    - state: present
      key: /path/to/key.gpg

```

```yaml
linux_resources:
    - module: rpm_key
      state: present
      key: http://apt.sw.be/RPM-GPG-KEY.dag.txt
    - module: rpm_key
      state: present
      key: /path/to/key.gpg

```

For more examples and information on options see
[`ansible.builtin.rpm_key`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/rpm_key_module.html).
#### `runit`: Manage runit services

```yaml
linux_runit:
    - name: dnscache
      state: started

```

```yaml
linux_resources:
    - module: runit
      enabled: true
      name: dnscache
      service_dir: /var/service
      service_src: /etc/sv
      state: started

```

For more examples and information on options see
[`community.general.runit`](https://docs.ansible.com/ansible/latest/collections/community/general/runit_module.html).
#### `script`: Runs a local script on a remote node after transferring it

```yaml
linux_script:
  - cmd: /some/local/script.sh --some-argument 1234

```

```yaml
linux_resources:
  - module: script
    chdir: /path/to/script/directory

```

For more examples and information on options see
[`ansible.builtin.script`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/script_module.html).
#### `seboolean`: Toggles SELinux booleans

```yaml
linux_seboolean:
  - name: httpd_can_network_connect
    state: true
    persistent: true
  - name: samba_enable_home_dirs
    state: false

```

```yaml
linux_resources:
  - module: seboolean
    name: httpd_can_network_connect
    state: true
    persistent: true
  - module: seboolean
    name: samba_enable_home_dirs
    state: false

```

For more examples and information on options see
[`ansible.posix.seboolean`](https://docs.ansible.com/ansible/latest/collections/ansible/posix/seboolean_module.html).
#### `sefcontext`: Manages SELinux file context mapping definitions

```yaml
linux_sefcontext:
  - target: '/srv/git_repos(/.*)?'
    setype: httpd_sys_rw_content_t
    state: present

```

```yaml
linux_resources:
  - module: sefcontext
    target: /srv/containers
    substitute: /var/lib/containers
    state: present

```

For more examples and information on options see
[`community.general.sefcontext`](https://docs.ansible.com/ansible/latest/collections/community/general/sefcontext_module.html).
#### `selinux`: Change policy and state of SELinux

```yaml
selinux:
  - policy: targeted
    state: enforcing

```

```yaml
linux_resources:
  - module: selinux
    configfile: /etc/selinux/config
    state: disabled

```

For more examples and information on options see
[`ansible.posix.selinux`](https://docs.ansible.com/ansible/latest/collections/ansible/posix/selinux_module.html).
#### `selinux_permissive`: Change permissive domain in SELinux policy

```yaml
selinux_permissive:
    - domain: httpd_t
      permissive: true

```

```yaml
linux_resources:
    - module: selinux_permissive
      domain: httpd_t
      permissive: true

```

For more examples and information on options see
[`community.general.selinux_permissive`](https://docs.ansible.com/ansible/latest/collections/community/general/selinux_permissive_module.html).
#### `selogin`: Manages linux user to SELinux user mapping

```yaml
linux_selogin:
  - login: guest
    seuser: user1
    state: present
  - login: developer
    seuser: user2
    state: absent

```

```yaml
linux_resources:
  - module: selogin
    login: admin
    seuser: root
    state: present
  - module: selogin
    login: user1
    seuser: standard
    state: present

```

For more examples and information on options see
[`community.general.selogin`](https://docs.ansible.com/ansible/latest/collections/community/general/selogin_module.html).
#### `service`: Manage services

```yaml
linux_service:
  - name: httpd
    state: started

```

```yaml
linux_resources:
  - module: service
    name: nginx
    state: restarted

```

For more examples and information on options see
[`ansible.builtin.service`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/service_module.html).
#### `shell`: Execute shell commands on targets

```yaml
linux_shell:
  - cmd: somescript.sh >> somelog.txt

```

```yaml
linux_resources:
  - module: shell
    cmd: somescript.sh >> somelog.txt
    chdir: somedir/
    creates: somelog.txt

```

For more examples and information on options see
[`ansible.builtin.shell`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/shell_module.html).
#### `shutdown`: Shut down a machine

```yaml
linux_shutdown:
  - delay: 60
    msg: "Shutting down for maintenance"

```

```yaml
linux_resources:
  - module: shutdown
    delay: 30

```

For more examples and information on options see
[`community.general.shutdown`](https://docs.ansible.com/ansible/latest/collections/community/general/shutdown_module.html).
#### `slurp`: Slurps a file from remote nodes

```yaml
linux_slurp:
  - src: /var/log/syslog
  - src: /etc/hosts

```

```yaml
linux_resources:
  - module: slurp
    src: /var/log/messages
  - module: slurp
    src: /etc/hostname

```

For more examples and information on options see
[`ansible.builtin.slurp`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/slurp_module.html).
#### `snap`: Manages snaps

```yaml
linux_snap:
    - name:
        - foo
        - bar

```

```yaml
linux_resources:
    - module: snap
      name:
        - foo
        - bar
      classic: true
      state: present

```

For more examples and information on options see
[`community.general.snap`](https://docs.ansible.com/ansible/latest/collections/community/general/snap_module.html).
#### `snap_alias`: Manages snap aliases

```yaml
linux_snap_alias:
  - name: hello-world
    alias: hw

```

```yaml
linux_resources:
  - module: snap_alias
    name: hello-world
    alias: hw

```

For more examples and information on options see
[`community.general.snap_alias`](https://docs.ansible.com/ansible/latest/collections/community/general/snap_alias_module.html).
#### `ssh_config`: Manage SSH config for user

```yaml
linux_ssh_config:
  - user: akasurde
    host: "example.com"
    hostname: "github.com"
    identity_file: "/home/akasurde/.ssh/id_rsa"
    port: '2223'
    state: present

```

```yaml
linux_resources:
  - module: ssh_config
    forward_agent: true
    host: "example.com"

```

For more examples and information on options see
[`community.general.ssh_config`](https://docs.ansible.com/ansible/latest/collections/community/general/ssh_config_module.html).
#### `sudoers`: Manage sudoers files

```yaml
linux_sudoers:
  - name: allow-backup
    state: present
    user: backup
    commands: /usr/local/bin/backup

```

```yaml
linux_resources:
  - module: sudoers
    name: allow-mary
    state: present
    user: mary
    commands: ALL

```

For more examples and information on options see
[`community.general.sudoers`](https://docs.ansible.com/ansible/latest/collections/community/general/sudoers_module.html).
#### `synchronize`: A wrapper around rsync to make common tasks in your playbooks quick and easy

```yaml
linux_synchronize:
  - src: some/relative/path
    dest: /some/absolute/path

```

```yaml
linux_resources:
  - module: synchronize
    src: some/relative/path
    dest: /some/absolute/path

```

For more examples and information on options see
[`ansible.posix.synchronize`](https://docs.ansible.com/ansible/latest/collections/ansible/posix/synchronize_module.html).
#### `sysctl`: Manage entries in sysctl.conf.

```yaml
linux_sysctl:
  - name: vm.swappiness
    value: '5'
    state: present

  - name: kernel.panic
    state: absent
    sysctl_file: /etc/sysctl.conf

  - name: kernel.panic
    value: '3'
    sysctl_file: /tmp/test_sysctl.conf
    reload: false

```

```yaml
linux_resources:
  - module: sysctl
    ignoreerrors: true
    name: net.ipv4.ip_forward
    state: present

```

For more examples and information on options see
[`ansible.posix.sysctl`](https://docs.ansible.com/ansible/latest/collections/ansible/posix/sysctl_module.html).
#### `syslogger`: Log messages in the syslog

```yaml
linux_syslogger:
    - msg: "I will end up as daemon.info"

```

```yaml
linux_resources:
    - module: syslogger
      facility: daemon
      msg: "I want to believe"

```

For more examples and information on options see
[`community.general.syslogger`](https://docs.ansible.com/ansible/latest/collections/community/general/syslogger_module.html).
#### `systemd`: Manage systemd units

```yaml
linux_systemd:
  - name: httpd
    state: started

```

```yaml
linux_resources:
  - module: systemd
    name: httpd
    state: restarted
    daemon_reload: true

```

For more examples and information on options see
[`ansible.builtin.systemd`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/systemd_module.html).
#### `systemd_service`: Manage systemd units

```yaml
linux_systemd_service:
  - name: httpd
    state: started
  - name: cron
    state: stopped

```

```yaml
linux_resources:
  - module: systemd_service
    name: httpd
    state: reloaded

```

For more examples and information on options see
[`ansible.builtin.systemd_service`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/systemd_service_module.html).
#### `sysvinit`: Manage SysV services.

```yaml
linux_sysvinit:
  - name: apache2
    state: started
    enabled: yes

```

```yaml
linux_resources:
  - module: sysvinit
    name: apache2
    state: started
    enabled: yes

```

For more examples and information on options see
[`ansible.builtin.sysvinit`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/sysvinit_module.html).
#### `tempfile`: Creates temporary files and directories

```yaml
linux_tempfile:
  - state: directory
    suffix: build

```

```yaml
linux_resources:
  - module: tempfile
    path: "/tmp"
    prefix: "ansible_"

```

For more examples and information on options see
[`ansible.builtin.tempfile`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/tempfile_module.html).
#### `template`: Template a file out to a target host

```yaml
linux_template:
  - src: /mytemplates/foo.j2
    dest: /etc/file.conf
    owner: bin
    group: wheel
    mode: '0644'

```

```yaml
linux_resources:
  - module: template
    src: /mytemplates/foo.j2
    dest: /etc/file.conf
    owner: bin
    group: wheel
    mode: '0644'

```

For more examples and information on options see
[`ansible.builtin.template`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/template_module.html).
#### `timezone`: Configure timezone setting

```yaml
linux_timezone:
  - name: Asia/Tokyo

```

```yaml
linux_resources:
  - module: timezone
    hwclock: UTC
    name: Europe/London

```

For more examples and information on options see
[`community.general.timezone`](https://docs.ansible.com/ansible/latest/collections/community/general/timezone_module.html).
#### `ufw`: Manage firewall with UFW

```yaml
linux_ufw:
  - rule: allow
    name: OpenSSH
  - rule: deny
    port: '53'

```

```yaml
linux_resources:
  - module: ufw
    default: allow
    direction: in

```

For more examples and information on options see
[`community.general.ufw`](https://docs.ansible.com/ansible/latest/collections/community/general/ufw_module.html).
#### `unarchive`: Unpacks an archive after (optionally) copying it from the local machine

```yaml
linux_unarchive:
  - src: foo.tgz
    dest: /var/lib/foo

```

```yaml
linux_resources:
  - module: unarchive
    src: foo.tgz
    dest: /var/lib/foo

```

For more examples and information on options see
[`ansible.builtin.unarchive`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/unarchive_module.html).
#### `uri`: Interacts with webservices

```yaml
linux_uri:
  - url: http://www.example.com

```

```yaml
linux_resources:
  - module: uri
    url: http://www.example.com

```

For more examples and information on options see
[`ansible.builtin.uri`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/uri_module.html).
#### `user`: Manage user accounts

```yaml
linux_user:
  - name: johnd
    comment: John Doe
    uid: 1040
    group: admin

```

```yaml
linux_resources:
  - module: user
    state: present
    name: johnd
    shell: /bin/bash
    groups:
      - admins
      - developers
    append: true

```

For more examples and information on options see
[`ansible.builtin.user`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/user_module.html).
#### `wait_for`: Waits for a condition before continuing

```yaml
linux_wait_for:
  - path: /tmp/foo
    search_regex: completed
  - path: /var/lock/file.lock
    state: absent
  - path: /proc/3466/status
    state: absent

```

```yaml
linux_resources:
  - module: wait_for
    path: /tmp/foo
    delay: 10
    msg: Timeout to find file /tmp/foo
  - module: wait_for
    port: 22
    host: '{{ (ansible_ssh_host|default(ansible_host))|default(inventory_hostname) }}'
    search_regex: OpenSSH
    delay: 10
    connection: local

```

For more examples and information on options see
[`ansible.builtin.wait_for`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/wait_for_module.html).
#### `wait_for_connection`: Waits until remote system is reachable/usable

```yaml
linux_wait_for_connection:
  - timeout: 300
    delay: 60
  - timeout: 600
    delay: 120

```

```yaml
linux_resources:
  - module: wait_for_connection
    connect_timeout: 10
    sleep: 2

```

For more examples and information on options see
[`ansible.builtin.wait_for_connection`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/wait_for_connection_module.html).
#### `xml`: Manage bits and pieces of XML files or strings

```yaml
linux_xml:
  - path: /foo/bar.xml
    xpath: /business/rating/@subjective
    state: absent
  - path: /foo/bar.xml
    xpath: /business/rating
    value: 11

```

```yaml
linux_resources:
  - module: xml
    path: /foo/bar.xml
    xpath: /business/beers
    add_children:
      - beer: Old Rasputin
      - beer: Old Motor Oil
      - beer: Old Curmudgeon

  - module: xml
    path: /foo/bar.xml
    xpath: '/business/beers/beer[text()="Rochefort 10"]'
    insertbefore: true
    add_children:
      - beer: Old Rasputin
      - beer: Old Motor Oil
      - beer: Old Curmudgeon

```

For more examples and information on options see
[`community.general.xml`](https://docs.ansible.com/ansible/latest/collections/community/general/xml_module.html).
#### `yum`: Manages packages with the I(yum) package manager

```yaml
linux_yum:
  - name: httpd
    state: latest

```

```yaml
linux_resources:
  - module: yum
    name: httpd
    state: present

```

For more examples and information on options see
[`ansible.builtin.yum`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/yum_module.html).
#### `yum_repository`: Add or remove YUM repositories

```yaml
linux_yum_repository:
  - name: epel
    description: EPEL YUM repo
    baseurl: https://download.fedoraproject.org/pub/epel/$releasever/$basearch/

```

```yaml
linux_resources:
  - module: yum_repository
    name: epel
    description: EPEL YUM repo
    baseurl: https://download.fedoraproject.org/pub/epel/$releasever/$basearch/

```

For more examples and information on options see
[`ansible.builtin.yum_repository`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/yum_repository_module.html).
#### `yum_versionlock`: Locks / unlocks a installed package(s) from being updated by yum package manager

```yaml
linux_yum_versionlock:
  - name: httpd
  - name: nginx
linux_resources:
  - module: yum_versionlock
    name: httpd
    state: present
  - module: yum_versionlock
    name: nginx
    state: present

```

For more examples and information on options see
[`community.general.yum_versionlock`](https://docs.ansible.com/ansible/latest/collections/community/general/yum_versionlock_module.html).
#### `zfs`: Manage zfs

```yaml
linux_zfs:
    - name: rpool/myfs
      state: present
      extra_zfs_properties:
        setuid: 'off'

```

```yaml
linux_resources:
    - module: zfs
      extra_zfs_properties:
        volsize: 10M
      name: rpool/myvol
      state: present

```

For more examples and information on options see
[`community.general.zfs`](https://docs.ansible.com/ansible/latest/collections/community/general/zfs_module.html).
#### `zfs_delegate_admin`: Manage ZFS delegated administration (user admin privileges)

```yaml
linux_zfs_delegate_admin:
  - name: rpool/myfs
    users:
      - adm
    permissions:
      - allow
      - unallow

```

```yaml
linux_resources:
  - module: zfs_delegate_admin
    name: rpool/myfs
    everyone: true
    groups:
      - backup
    permissions:
      - send

```

For more examples and information on options see
[`community.general.zfs_delegate_admin`](https://docs.ansible.com/ansible/latest/collections/community/general/zfs_delegate_admin_module.html).
#### `zfs_facts`: Gather facts about ZFS datasets

```yaml
linux_zfs_facts:
  - dataset: rpool/export/home

```

```yaml
linux_resources:
  - module: zfs_facts
    depth: 0
    name: rpool/export/home

```

For more examples and information on options see
[`community.general.zfs_facts`](https://docs.ansible.com/ansible/latest/collections/community/general/zfs_facts_module.html).
<!-- end supported_modules -->

## Handlers

Handlers are tasks that are only run when notified by other tasks. They are
typically used for service restarts or other post-configuration actions. Below
is a list of handlers included in this role:

### Handlers List

#### `firewalld-reloaded`: Reloads firewalld

#### `systemctl-daemon-reload`: Run the equivalent of systemctl daemon-reload

Ensures that systemd is aware of new or changed unit files.

### Usage Examples

To trigger a handler, a task or the `linux_resources` variable must include the `notify` directive. Here's an example:

```yaml
linux_resources:
  - name: Open 80
    type: firewalld
    port:  80/tcp
    zone: public
    permanent: yes
    state: enabled
    notify: firewalld-reloaded
```

## Dependencies

<!--   A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles. -->

## Example Playbook

<!--   Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too: -->

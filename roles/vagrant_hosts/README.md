# Ansible Role c2platform.core.vagrant_hosts

This Ansible role is integral to the
[C2 Platform Development Environment](https://c2platform.org/docs/concepts/dev/).
, facilitating the configuration of development setups as outlined in
[Manage Your Development Environment | C2 Platform](https://c2platform.org/docs/howto/dev-environment/).
It dynamically reads Vagrantfile.yml to manage hosts file entries across both
Linux and Windows Vagrant guest nodes.

- [Requirements](#requirements)
- [Role Variables](#role-variables)
  - [`vagrant_hosts_host_state`: State attribute for hosts file entries on the Vagrant host](#vagrant_hosts_host_state-state-attribute-for-hosts-file-entries-on-the-vagrant-host)
  - [`vagrant_hosts_guest_state`: State attribute for hosts file entries of Vagrant guest node](#vagrant_hosts_guest_state-state-attribute-for-hosts-file-entries-of-vagrant-guest-node)
  - [`vagrant_hosts_prefix`: Used for the `marker` attribute of `ansible.builtin.blockinfile`.](#vagrant_hosts_prefix-used-for-the-marker-attribute-of-ansiblebuiltinblockinfile)
  - [`vagrant_hosts_content_win`: Override default `hosts` content](#vagrant_hosts_content_win-override-default-hosts-content)
- [Dependencies](#dependencies)
- [Example](#example)

## Requirements

<!-- Any pre-requisites that may not be covered by Ansible itself or the role should be mentioned here. For instance, if the role uses the EC2 module, it may be a good idea to mention in this section that the boto package is required. -->

## Role Variables

<!--  A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well. -->

### `vagrant_hosts_host_state`: State attribute for hosts file entries on the Vagrant host

Default value is `absent`. Change to `present` to also update etc `/etc/hosts`
file of the Vagrant host.

### `vagrant_hosts_guest_state`: State attribute for hosts file entries of Vagrant guest node

Default value is `present`. Change to `absent` if you want don't want to add or
want to remove entries.

### `vagrant_hosts_prefix`: Used for the `marker` attribute of `ansible.builtin.blockinfile`.

The variable `vagrant_hosts_prefix` with default value `c2d` is used on
Linux guests to update the hosts file using `ansible.builtin.blockinfile`
module. The variable is used for the `marker` attribute. This is useful if you
are updating the hosts file of your Vagrant host and want to have multiple
environments in there at once. This is only useful if you are updating the hosts
file of the Vagrant host ( and so you have `vagrant_hosts_host_state` with
non-default value `true` ).

### `vagrant_hosts_content_win`: Override default `hosts` content

Using optional variable `vagrant_hosts_content_win` you can also specify
the content for the `hosts` file on Windows targets.

```yaml
vagrant_hosts_content_win: |
  # hosts file managed by Ansible
  {{ vagrant_hosts_content }}
```

## Dependencies

<!--   A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles. -->

## Example

<!--   Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too: -->


Here's a practical example of how to implement this role within the
[Ansible Inventory Project for the RWS GIS Platform](https://c2platform.org/docs/gitlab/c2platform/rws/ansible-gis/)
, including a snippet of the `/etc/hosts` file configuration for the `gsd-rproxy1` host.

```
---
root@gsd-rproxy1:~# cat /etc/hosts
# This file is managed by Ansible, all changes will be lost.
127.0.0.1 localhost.localdomain localhost
10.94.57.32 gsd-rproxy1 gsd-rproxy1


# The following lines are desirable for IPv6 capable hosts
::1     ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
## BEGIN VAGRANT / ANSIBLE MANAGED BLOCK C2D
1.1.5.205 gsd-rproxy1 gsd-rproxy1.internal.c2platform.org c2platform.org gis.c2platform.org webproxy.c2platform.org checkmk.c2platform.org
1.1.5.206 gsd-db1 gsd-db1.internal.c2platform.org
1.1.5.207 gsd-tomcat1 gsd-tomcat1.internal.c2platform.org
1.1.8.207 gsd-tomcat2 gsd-tomcat2.internal.c2platform.org
1.1.8.100 gsd-agserver1 gsd-agserver1.internal.c2platform.org
1.1.8.103 gsd-agportal1 gsd-agportal1.internal.c2platform.org
1.1.8.101 gsd-agpro1 gsd-agpro1.internal.c2platform.org
1.1.8.112 gsd-ansible-file-share1 gsd-ansible-file-share1.internal.c2platform.org
1.1.8.110 gsd-ansible-repo gsd-ansible-repo.internal.c2platform.org
1.1.8.113 gsd-ansible-download1 gsd-ansible-download1.internal.c2platform.org
1.1.8.114 gsd-ansible-download2 gsd-ansible-download2.internal.c2platform.org
1.1.8.115 gsd-ansible-download3 gsd-ansible-download3.internal.c2platform.org
1.1.8.116 gsd-python gsd-python.internal.c2platform.org
1.1.8.105 gsd-ansible gsd-ansible.internal.c2platform.org
1.1.8.106 gsd-fme-core gsd-fme-core.internal.c2platform.org
1.1.8.107 gsd-fme-engine gsd-fme-engine.internal.c2platform.org
1.1.8.108 gsd-ad gsd-ad.internal.c2platform.org
1.1.8.109 gsd-env-test gsd-env-test.internal.c2platform.org
1.1.8.111 gsd-ca-server gsd-ca-server.internal.c2platform.org
1.1.8.117 gsd-ca-server-client gsd-ca-server-client.internal.c2platform.org
1.1.5.208 gsd-checkmk-server gsd-checkmk-server.internal.c2platform.org
## END VAGRANT / ANSIBLE MANAGED BLOCK C2D
```

# Ansible Role c2platform.core.cacerts2_client

Use this role to generate OwnCA certificates. This role works together with
[`cacerts2`](../cacerts2/README.md).

- [Requirements](#requirements)
- [Role Variables](#role-variables)
  - [`cacerts2_client_cacerts2_certificates`](#cacerts2_client_cacerts2_certificates)
- [Dependencies](#dependencies)
- [Example Playbook](#example-playbook)

## Requirements

<!-- Any pre-requisites that may not be covered by Ansible itself or the role should be mentioned here. For instance, if the role uses the EC2 module, it may be a good idea to mention in this section that the boto package is required. -->

## Role Variables

<!--  A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well. -->

### `cacerts2_client_cacerts2_certificates`

See for an example the configuration in
[`host_vars/gsd-ownca.yml`](https://gitlab.com/c2platform/rws/ansible-gis/-/blob/master/host_vars/gsd-ownca.yml?ref_type=heads) and play [`plays/mw/ownca.yml`](https://gitlab.com/c2platform/rws/ansible-gis/-/blob/master/plays/mw/ownca.yml?ref_type=heads)


## Dependencies

<!--   A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles. -->

## Example Playbook

<!--   Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too: -->

The play below combines the OwnCA Server and Client in one play.

```yaml
---
- name: CentOS OwnCA Server and Certificate Client
  hosts: cacerts_server_centos
  become: true

  roles:
    - { role: c2platform.core.common, tags: ["common"] }
    - { role: c2platform.core.cacerts2, tags: ["ownca"] }
    - { role: c2platform.core.cacerts2_client, tags: ["certificates"] }
```
# Ansible Role c2platform.core.mount

Configure and manage mount points [ansible.posix.mount](https://docs.ansible.com/ansible/latest/collections/ansible/posix/mount_module.html). This role is default included and enabled in the [c2platform.core.common](https://gitlab.com/c2platform/ansible-collection-core/-/blob/master/roles/common/README.md) role.

<!-- MarkdownTOC levels="2,3" autolink="true" -->

- [Requirements](#requirements)
- [Role Variables](#role-variables)
  - [mount_points](#mount_points)
  - [mount_groups_disabled](#mount_groups_disabled)
  - [common_roles_disabled](#common_roles_disabled)
- [Dependencies](#dependencies)
- [Example Playbook](#example-playbook)

<!-- /MarkdownTOC -->

## Requirements

<!-- Any pre-requisites that may not be covered by Ansible itself or the role should be mentioned here. For instance, if the role uses the EC2 module, it may be a good idea to mention in this section that the boto package is required. -->

## Role Variables

<!--  A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well. -->

### mount_points

Use the dict `mount_points` to configure / manage one or more mount points. Each item the grouped lists manages a mount point using [ansible.posix.mount](https://docs.ansible.com/ansible/latest/collections/ansible/posix/mount_module.html) module. The example below creates two mount points `/backup` and `/software`.

```yaml
mount_points:
  backup:
    - path: /backup
      src: nfs.example.com:/volume1/backup
      opts: rw
      fstype: nfs
      state: mounted
  software:
    - path: /software
      src: nfs.example.com:/volume1/software
      opts: rw
      fstype: nfs
      state: mounted
```

### mount_groups_disabled

Use the list `common_roles_disabled` to disable one or more grouped lists of mounts points, the example below will result in only `/software` mount to be created, because the `backup` group is disabled.

```yaml
mount_groups_disabled: ['backup']
```

### common_roles_disabled

This role is default also included in [c2platform.core.common](https://gitlab.com/c2platform/ansible-collection-core/-/blob/master/roles/common/README.md) and so it is also possible to disable this role altogether with:

```yaml
common_roles_disabled: ['mount']
```

## Dependencies

<!--   A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles. -->

## Example Playbook

<!--   Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too: -->

```yaml
---
- name: Whatever
  hosts: hosts
  become: yes

  roles:
    - { role: c2platform.core.mount }
```

Because this role is also included in [c2platform.core.common](https://gitlab.com/c2platform/ansible-collection-core/-/blob/master/roles/common/README.md) role the following is also possible.

```yaml
---
- name: Whatever
  hosts: hosts
  become: yes

  roles:
    - { role: c2platform.core.common }
```

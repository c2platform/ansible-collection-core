# Ansible Role c2platform.core.env_test

This Ansible role facilitates various tests of an environment for example, in
its current release verify network connectivity a simple socket listener /
server using module `c2platform.core.env_test_server` on a designated node and
port, then sending messages from specified nodes to this listener using
`c2platform.core.env_test_client`. This setup helps in verifying network
connectivity and message handling between different nodes in your
infrastructure.

- [Requirements](#requirements)
- [Role Variables](#role-variables)
  - [`env_test`](#env_test)
- [Dependencies](#dependencies)
- [Example Playbook](#example-playbook)

## Requirements

<!-- Any pre-requisites that may not be covered by Ansible itself or the role should be mentioned here. For instance, if the role uses the EC2 module, it may be a good idea to mention in this section that the boto package is required. -->

## Role Variables

<!--  A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well. -->

### `env_test`

The `env_test` variable is designed to facilitate the configuration of network
tests within the environment. The example provided below illustrates a typical
test setup:

1. Initializes a listener on the `gsd-rproxy1` node on port `5601`, `5602` and
   `5603`.
1. Dispatches test messages from `gsd-tomcat1`, `gsd-db1` and `gsd-env-test`
   nodes targeting port `5601`, `5602` and `5603`, ensuring the connectivity and
   integrity of the responses.
1. Evaluates the accessibility of port `80`, `443`, `1080`.

```yaml
env_tests:
  - id: test-2
    name: Test connectivity to gsd-rproxy1
    timeout_seconds: 120
    log_file: /vagrant/logs/env_test_listener_{node}_{port}.log
    servers:
      - node: gsd-rproxy1
        ip: 1.1.5.205
    clients:
      - gsd-tomcat1
      - gsd-db1
      - gsd-rproxy1
    clients_win:
      - gsd-env-test
    ports: [5601, 5602, 5603]
    ports_other: [80, 443, 1080]
```

This setup is flexible enough to include tests on ports that are in use. The
`env_tests` variable however can also be used to configure services to
temporarily halted and then restarted to perform necessary tests. The following
configuration showcases the use of the `stop_start` attribute for controlling
service states during testing.

```yaml
env_tests:
  - id: test-2
    name: Test connectivity and manage Apache2 service
    timeout_seconds: 120
    log_file: /vagrant/logs/env_test_listener_{node}_{port}.log
    servers:
      - node: gsd-rproxy1
        ip: 1.1.5.205
    stop_start:
      - service: apache2
        wait_for: [80, 443, 1080]
    clients:
      - gsd-tomcat1
      - gsd-db1
      - gsd-rproxy1
    clients_win:
      - gsd-env-test
    ports: [5601, 5602, 5603, 80, 443, 1080]
```

## Dependencies

<!--   A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles. -->

## Example Playbook

<!--   Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too: -->

```yaml
---
- name: Environment Tests
  hosts: gsd-rproxy1

  roles:
    - { role: c2platform.core.env_test, tags: ["env_test"]}

  vars:
    env_test_timeout_seconds: 15
    env_tests:
      - id: test-1
        name: Test connectivity to gsd-rproxy1
        timeout_seconds: 120
        log_file: /vagrant/logs/env_test_listener_{node}_{port}.log
        servers:
          - node: gsd-rproxy1
            ip: 1.1.5.205
        clients:
          - gsd-tomcat1
          - gsd-db1
          - gsd-rproxy1
        clients_win:
          - gsd-env-test
        ports: [5601, 5602, 5603]
        ports_other: [80, 443, 1080]
      - id: test-2
        name: Test connectivity and manage Apache2 service
        timeout_seconds: 120
        log_file: /vagrant/logs/env_test_listener_{node}_{port}.log
        servers:
          - node: gsd-rproxy1
            ip: 1.1.5.205
        stop_start:
          - service: apache2
            wait_for: [80, 443, 1080]
        clients:
          - gsd-tomcat1
          - gsd-db1
          - gsd-rproxy1
        clients_win:
          - gsd-env-test
        ports: [5601, 5602, 5603, 80, 443, 1080]
      - id: test-3
        name: Test connectivity to Apache2
        timeout_seconds: 120
        log_file: /vagrant/logs/env_test_listener_{node}_{port}.log
        servers:
          - node: gsd-rproxy1
            ip: 1.1.5.205
        clients:
          - gsd-tomcat1
          - gsd-db1
          - gsd-rproxy1
        clients_win:
          - gsd-env-test
        other_ports: [80, 443, 1080]
```
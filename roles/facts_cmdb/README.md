# Ansible Role c2platform.core.facts_cmdb

This Ansible role can be used to gather facts for `ansible-cmdb`.

- [Requirements](#requirements)
- [Role Variables](#role-variables)
- [`facts_cmdb_dest_node`, `facts_cmdb_dest_dir`: Destination node and directory](#facts_cmdb_dest_node-facts_cmdb_dest_dir-destination-node-and-directory)
- [`facts_cmdb_tmp_dir`: Temp directory](#facts_cmdb_tmp_dir-temp-directory)
- [`facts_cmdb_tmp_dir_delete` and `facts_cmdb_dest_dir_delete`](#facts_cmdb_tmp_dir_delete-and-facts_cmdb_dest_dir_delete)
- [Dependencies](#dependencies)
- [Example Playbook](#example-playbook)

## Requirements

<!-- Any pre-requisites that may not be covered by Ansible itself or the role should be mentioned here. For instance, if the role uses the EC2 module, it may be a good idea to mention in this section that the boto package is required. -->

## Role Variables

<!--  A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well. -->

## `facts_cmdb_dest_node`, `facts_cmdb_dest_dir`: Destination node and directory

Gathered facts of a host are stored on `localhost` in a temporary directory
`facts_cmdb_tmp_dir` and then moved to `facts_cmdb_dest_dir` on the destination
node configured using `facts_cmdb_dest_node`.

## `facts_cmdb_tmp_dir`: Temp directory

Variable `facts_cmdb_tmp_dir` is used to configured the directory on the control node ( localhost ) where facts are stored before being copied to destination node configured with `facts_cmdb_dest_node`.

## `facts_cmdb_tmp_dir_delete` and `facts_cmdb_dest_dir_delete`

The variables `facts_cmdb_tmp_dir_delete` and `facts_cmdb_dest_dir_delete` can
be used to respectively delete the temporary directory on the host and the
destination directory on the destination node.

## Dependencies

<!--   A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles. -->

## Example Playbook

<!--   Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too: -->

```yaml
    - hosts: servers
      roles:
         - { role: c2platform.core.facts_cmdb }
```
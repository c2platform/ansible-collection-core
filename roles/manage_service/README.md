# Ansible Role: c2platform.core.manage_service

This Ansible role, `c2platform.core.manage_service`, is designed to automate
the process of stopping, starting, and restarting services on both Windows and
Linux systems using just two variables: `manage_service` and
`manage_service_routine`. These routines can be easily and flexibly orchestrated
across multiple nodes, accommodating a combination of Windows and Linux hosts.
In its current iteration, the role supports all tasks available in the Windows
role `c2platform.wincore.win` and the Linux role `c2platform.core.linux`.

- [Requirements](#requirements)
- [Role Variables](#role-variables)
  - [`manage_service`](#manage_service)
  - [`manage_service_routine`](#manage_service_routine)
- [Dependencies](#dependencies)
- [Example Playbook](#example-playbook)

## Requirements

<!-- Any pre-requisites that may not be covered by Ansible itself or the role should be mentioned here. For instance, if the role uses the EC2 module, it may be a good idea to mention in this section that the boto package is required. -->

## Role Variables

<!--  A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well. -->

### `manage_service`

The `manage_service` variable functions similarly to `win_resources` and
`linux_resources` in the respective Windows and Linux roles:
[`c2platform.wincore.win`](https://gitlab.com/c2platform/rws/ansible-collection-wincore/-/tree/master/roles/win) and
[`c2platform.core.linux`](https://gitlab.com/c2platform/ansible-collection-core/-/tree/master/roles/linux?ref_type=heads).

Please refer to their respective `README.md` files for more information on
supported modules.

This role provides additional or different variables for task items added to
the `manage_service` variable:

1. `hosts`: A list of target nodes, such as hostnames `[gsd-fme-core]` or
   using the `group` variable `"{{ groups['fme_core'] }}"`.
2. `routines`: A list of routines that the task item is part of, such as
   `stop`, `start`, `restart`, `start-maintenance`, or `stop-maintenance`.
   Custom routine names are permissible, as you can select the desired routine
   using the [`manage_service_routine`](#manage_service_routine) variable.
3. `role`: The name of the Ansible role. Currently, the supported roles are
   `c2platform.core.linux` and `c2platform.wincore.win`.
4. `tasks_from`: The tasks file or module to use, for instance, `win_service`
   would select the `win/tasks/win_service.yml` file, which is part of the
   Windows role `c2platform.wincore.win`.

### `manage_service_routine`

The `manage_service_routine` variable is a string that determines which routine
to run. For example, `manage_service_routine: start`.

## Dependencies

<!--   A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles. -->

- Windows role: `c2platform.wincore.win`.
- Linux role: `c2platform.core.linux`.

## Example Playbook

<!--   Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too: -->

Below is an example of how to use the `c2platform.core.manage_service` role in
an Ansible playbook, with variables provided as parameters:

```yaml
---
- name: Stop and Start FME Core & Engine
  hosts: localhost

  roles:
    - { role: c2platform.core.manage_service }

  vars:
    win_roles: []
    manage_service_routine: start
    manage_service:
      00_ping:
        - name: Ping Hosts
          defaults:
            routines: [stop, restart]
            role: c2platform.wincore.win
            tasks_from: win_ping
          resources:
            - name: FME
              hosts: [gsd-fme-core]
              # hosts: "{{ groups['fme_core'] }}"
            - name: Reverse Proxy
              hosts: [gsd-rproxy1]
              role: c2platform.core.linux
              tasks_from: ping
      01_stop:
        - name: Stop Core, Engine
          defaults:
            state: stopped
            routines: [stop, restart]
            role: c2platform.wincore.win
            tasks_from: win_service
          resources:
            - name: apache2
              hosts: [gsd-rproxy1]
              role: c2platform.core.linux
              tasks_from: service
              become: true
            - name: FME Flow Core
              hosts: [gsd-fme-core]
              # hosts: "{{ groups['fme_core'] }}"
            - name: FME-SERVER-WAS
              hosts: [gsd-fme-core]
      02_start:
        - name: Start Core, Engine
          defaults:
            state: started
            routines: [start, restart]
            role: c2platform.wincore.win
            tasks_from: win_service
          resources:
            - name: FME Flow Core
              hosts: [gsd-fme-core]
              # hosts: "{{ groups['fme_core'] }}"
            - name: FME-SERVER-WAS
              hosts: [gsd-fme-core]
              # hosts: "{{ groups['fme_core'] }}"
            # - name: FME Flow Engines
            #   hosts: "{{ groups['fme_engine'] }}"
            - name: apache2
              hosts: [gsd-rproxy1]
              role: c2platform.core.linux
              tasks_from: service
              become: true
```
# Ansible Collection - c2platform.core

[![Pipeline Status](https://gitlab.com/c2platform/ansible-collection-core/badges/master/pipeline.svg?style=flat-square&key_text=Pipeline+Status&key_width=90)](https://gitlab.com/c2platform/ansible-collection-core/-/pipelines)
[![Latest Release](https://gitlab.com/c2platform/ansible-collection-core/-/badges/release.svg?style=flat-square)](https://gitlab.com/c2platform/ansible-collection-core/-/pipelines)
[![Ansible Galaxy](https://img.shields.io/badge/Galaxy-_c2platform.core-blue.svg)](https://galaxy.ansible.com/ui/repo/published/c2platform/core/)

C2 Platform generic roles that are used by all or some other roles. These roles
typically don't create services / processes on target node but are dependencies
e.g. packages required by those roles. Or these roles help with Ansible
provisioning for example offers generic Ansible modules, filters etc.

## Roles

* [`linux`](./roles/linux) manage of Linux systems by leveraging +112 modules
  from the the `ansible.builtin`, `ansible.posix`, and `community.general`
  collections.
* [`server_update`](./roles/server_update) update yum / apt cache and /or
  upgrade packages.
* [`bootstrap`](./roles/bootstrap) bootstrap your nodes with os and pip
  packages.
* [`mount`](./roles/mount) configure / manage mount points.
* [`secrets`](./roles/secrets) workaround for lack of support for vault when
  using AWX.
* [`os_trusts`](./roles/os_trusts) Manage OS trust store.
* [`cacerts2`](./roles/cacerts2) create your own smallCA.
* [`cacerts2_client`](./roles/cacerts2_client) generate ownca certificates.
* [`apt_repo`](./roles/apt_repo) add APT keys, repositories.
* [`yum`](./roles/yum) add / remove Yum repositories.
* [`files`](./roles/files) manage files, directories, ACL.
* [`users`](./roles/users) manage Linux accounts.
* [`service`](./roles/service) create systemd services.
* [`java`](./roles/java) install java, manage keystores.
* [`facts`](./roles/facts) gather facts.
* [`lcm`](./roles/lcm) facts for LCM operations for other roles to build upon.
* [`lvm`](./roles/lvm) manage data disks for roles using
  [LVM](https://en.wikipedia.org/wiki/Logical_Volume_Manager_%28Linux%29).
* [`rest`](./roles/rest) interact with REST webservices.
* [`postgresql_tasks`](./roles/postgresql_tasks) include tasks for PostgreSQL
  database operations.
* [`postgresql_client`](./roles/postgresql_client).
* [`manage_service`](./roles/manage_service) orchestrate stop, start and restart
  over multiple nodes.
* [`env_test`](./roles/env_test/) network test role.
* [`facts_cmdb`](./roles/facts_cmdb/) gather facts for `ansible-cmdb`.
* [`vagrant_hosts`](./roles/vagrant_hosts/) manage hosts file on Windows and
  Linux guests.

## Modules and Filters

For detailed information on the available modules and filters within this
collection, please refer to the
[Ansible Galaxy](https://galaxy.ansible.com/ui/repo/published/c2platform/core/docs/)
page.

You can of course also use the `ansible-doc` command to explore the
documentation:

```bash
ansible-doc -t module --list c2platform.core
ansible-doc -t filter --list c2platform.core
ansible-doc -t filter <FILTER_NAME>
ansible-doc -t module <MODULE_NAME>
```

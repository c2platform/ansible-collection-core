# Ansible Collection - c2platform.core

[![Pipeline Status](https://gitlab.com/c2platform/ansible-collection-core/badges/master/pipeline.svg?style=flat-square&key_text=Pipeline+Status&key_width=90)](https://gitlab.com/c2platform/ansible-collection-core/-/pipelines)
[![Latest Release](https://gitlab.com/c2platform/ansible-collection-core/-/badges/release.svg?style=flat-square)](https://gitlab.com/c2platform/ansible-collection-core/-/pipelines)
[![Ansible Galaxy](https://img.shields.io/badge/Galaxy-_c2platform.core-blue.svg)](https://galaxy.ansible.com/ui/repo/published/c2platform/core/)

See full [README](https://gitlab.com/c2platform/ansible-collection-core/-/blob/master/README.md).

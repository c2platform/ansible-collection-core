# CHANGELOG

## 1.0.25 ( )

* [`linux`](./roles/linux) label improvement for `apt_repository`.
* `.gitlab.ci.yml`: ee `0.1.7` → `0.1.25`
* [`postgresql_client`](./roles/postgresql_client) added variable
  `postgresql_client_apt_repository`.
* Removed deprecated role `alias`.
* [`linux`](./roles/linux) label improvement for `user`.
* [`lcm`](./roles/lcm) and [`lvm`](./roles/lvm) using FQCN for `lcm_roles`,
  `lvm_supported_roles` now.
* [`linux`](./roles/linux) added `become_user`.
* [`bootstrap`](./roles/bootstrap) added `retries`, `delay` to `snap` to make it
  more robust. To support `community.general` `10.2.0`, which causes message
  "Module failed with exception: list index out of range".
* [`linux`](./roles/linux) `linux_file` aliases `dest`, `name` for `path`.
* [`linux`](./roles/linux) fixed `linux_resource_groups_disabled` not working:
  `group` → `resource_group` .
* [`manage_service`](./roles/linux) new role for automating stop, start, restart
  routines.
* [`linux`](./roles/linux) added `delegate_to` and `become` to all tasks.
* [`yum`](./roles/yum) fix ansible lint issue.
* [`java`](./roles/java) added tags `install`, `application`, `java`, `java`.
* [`linux`](./roles/linux/) improved the `groups2items` filter to also be able
  to process items that use `defaults` and `resources` key.

## 1.0.24 ( 2024-12-19 )

* [`secrets`](./roles/secrets/) removed deprecation code / message for
  `common_secrets_dirs`.
* [`cacerts2`](./roles/cacerts2/) renamed tag `cacerts2-facts` to
  `cacerts2-always`.

## 1.0.23 ( 2024-11-13 )

* [`cacerts2`](./roles/cacerts2/) added tags `install`, `application`,
  `cacerts2` and `always`.
* Added `always` tag to `secrets` role.
* Added tags to roles `alias`, `apt_repo`, `bootstrap`, `hosts`, `lvm`,
  `os_trusts`, `secrets`, `vagrant_hosts`, `yum`.
* [`linux`](./roles/linux/) added tags `install`, `system`, `linux` to all tasks
  and added `tasks/main_base.yml` in order to be able to import the `linux` role
  without those tags.
* [`secrets`](./roles/secrets/) remove debug message.

## 1.0.22 ( 2024-06-26 )

* [`vagrant_hosts`](./roles/vagrant_hosts/) `vagrant_hosts_content` changed to
  dictionary with content for each node `prefix`.
* Re-add filter `add_attributes` back in. The filter was accidentally removed
  with filter documentation update in `1.0.19`.
* [`vagrant_hosts`](./roles/vagrant_hosts/) improved error message when
  `Vagrantfile.yml` is not found, fixed path for AWX / AAP in
  `vagrant_hosts_vagrantfile_yml` and added new var
  `vagrant_hosts_host_state_manage`.

## 1.0.21 ( 2024-06-04 )

* [`vagrant_hosts`](./roles/vagrant_hosts/) changed
  `vagrant_hosts_vagrantfile_yml` code so that it can also be a list.
* [`linux`](./roles/linux/) removed deprecated option `copy` from
  `ansible.builtin.unarchive` module code.
* [`vagrant_hosts`](./roles/vagrant_hosts/) new role.

## 1.0.20 ( 2024-05-31 )

* `c2platform.dev` `>=1.0.3`

## 1.0.19 ( 2024-05-27 )

* Added / fixed filter documentation.
* New filter `c2platform.core.dictlistsort` which sorts dictionary keys and
  their respective list values.
* `.gitlab-ci.yml` with `ansible-doc`.
* [`linux`](./roles/linux/) fix `package` `label`, `state` is optional with
  default value `present`.
* Added doc for `resources` filter.
* [`linux`](./roles/linux/) removed `loop_control` `label`.

## 1.0.18 ( 2024-05-06 )

* [`linux`](./roles/linux/) fix `linux_user` `notify` indentation wrong.
* [`linux`](./roles/linux/) removed label from `linux_apt`.
* [`linux`](./roles/linux/) added module `ansible.builtin.include_role`.
* [`linux`](./roles/linux/) fix `linux_include_role`.
* [`linux`](./roles/linux/) new variable `linux_roles`.
* [`bootstrap`](./roles/bootstrap/) improved, simplified by also enhanced the
  bootstrap role utilizing `c2platform.core.groups2items` and
  `c2platform.core.resources` filters.
* [`linux`](./roles/linux/) fix `notify` for `linux_user`.

## 1.0.17 ( 2024-05-02 )

* [`linux`](./roles/linux/) fix bug processing of `linux_<module>` variables.
* Enhanced filter `c2platform.core.resources` to fix bug in
  [`linux`](./roles/linux/) role.
* [`linux`](./roles/linux/) fix bug in `linux_resources_types` processing using
  new filter `c2platform.core.role_variable`.
* [`linux`](./roles/linux/) `include_role` disable `apply`.
* Improved documentation.
* [`secrets`](./roles/secrets/) using `loop_var` `secrets_dir_item` for loop and
  using `selectattr` to select only vaults that exists.
* Improved documentation.
* [`linux`](./roles/linux/) added module `ansible.builtin.fail`.

## 1.0.16 ( 2024-04-25 )

* Renamed filters from `to_backslashes`, `to_slashes` to `replace_backslashes`,
  `replace_slashes`.

## 1.0.15 ( 2024-04-18 )

* Doc update filters `to_slashes` and `to_backslashes`.

## 1.0.14 ( 2024-04-18 )

* New filters `to_slashes` and `to_backslashes`.

## 1.0.13 ( 2024-04-08)

* [`facts_cmdb`](./roles/facts_cmdb/) new role to gather facts for
  `ansible-cmdb`.
* Add new filter `facts_for_cmdb`.
* [`common`](./roles/common/) and [`hosts`](./roles/hosts/) `include` →
  `include_tasks`
* [`cacerts2`](./roles/cacerts2/) added `become` `cacerts2_delegate_become` to
  task **Copy to control node ( fetch )** ( `cert_deploy.yml` ).
* [`linux`](./roles/linux/) added `linux_include_role`.
* [`linux`](./roles/linux/) fix `linux_user` `notify`.

## 1.0.12 ( 2024-03-08 )

* [`bootstrap`](./roles/bootstrap/) added variable `bootstrap_environment`.
* [`linux`](./roles/linux/) added 112 modules from `ansible.builtin`,
  `ansible.posix` and `community.general`.
* [`linux`](./roles/linux/) added `linux_htpasswd`, `linux_lineinfile`.
* [`linux`](./roles/linux/) added handlers `firewalld-reloaded`,
  `systemctl-daemon-reload` and variables `linux_file`, `linux_seboolean`.

## 1.0.11 ( 2024-03-16 )

* [`env_test`](./roles/env_test/) various enhancements, fixes.

## 1.0.10 ( 2024-03-14 )

* [`env_test`](./roles/env_test/) readme en fix bug "could not find job".

## 1.0.9 ( 2024-03-14 )

* [`env_test`](./roles/env_test/) initial / concept network test role.
* [`linux`](./roles/linux) initial role for Linux hosts with ability to manage
  to hosts using various Ansible modules for example `linux_locale_gen`,
  `linux_timezone`, `linux_systemd_service`, `linux_copy`, `linux_firewalld`.

## 1.0.8 ( 2024-01-31 )

* Removed `win` role. Moved to `c2platform.wincore.win`.
* New filter `c2platform.core.resources`.
* [`cacerts2`](./roles/cacerts2) added to create / manage Java TrustStores.
* [`java`](./roles/java) added `java_home_mode` to set mode e.g. `a+rwx`.
* [`cacerts2`](./roles/cacerts2) added `cacerts2_delegate_become` to support
  delegation from Windows hosts.
* Removed obsolete `cacerts` role.
* [`cacerts2`](./roles/cacerts2) support deploy of SSL/TLS certificates and Java
  KeyStores to MS Windows hosts.
* [`cacerts2`](./roles/cacerts2) added ability to use external / manual
  certificates.
* [`cacerts2`](./roles/cacerts2) added ability to generate a Java KeyStore as
  additional distribution format for SSL / TLS certificates.
* [`cacerts2_client`](./roles/cacerts2_client) new role.

## 1.0.7  (  2023-11-22 )

* Improved [`secrets`](./roles/secrets) role.

## 1.0.6  ( 2023-10-12 )

* Fix filter `c2platform.core.groups2items`.

## 1.0.5 ( 2023-10-09 )

* Task **Snap package** in [`bootstrap`](./roles/bootstrap) more robust.

## 1.0.4 ( 2023-10-04 )

* New filter filter `c2platform.core.product`.

## 1.0.3 ( 2023-10-01 )

* Fix filter `c2platform.core.groups2items`.

## 1.0.2 ( 2023-08-14 )

* Improved filter `c2platform.core.groups2items`. Updated documentation. Now
  supports list, dicts and priority sorting of keys.

## 1.0.1 ( 2023-08-10 )

* [`lcm`](./roles/lcm) remove `awx` from `lcm_roles`.
* [`common`](./roles/common) removed dependencies on `ajsalminen.hosts`,
  `arillso.logrotate` and `stouts.sudo`.
* [`common`](./roles/common) removed creating Git repo with `common_git_repos`.
* Fixed Ansible Linter issues.

## 1.0.0 ( 2023-06-16 )

* [`bootstrap`](./roles/bootstrap) added `disable_gpg_check` attribute so we can
  install for example VS Code on Red Hat 8 using an URL.
* [`win`](./roles/win) deprecated. This role moved to
  [`c2platform.wincore`](https://gitlab.com/c2platform/rws/ansible-collection-wincore).

## 0.2.12 ( 2023-05-22 )

* [`win`](./roles/win) added modules `win_firewall` and `win_firewall_rules`.
* [`yum`](./roles/yum) add / remove Yum repositories using list
  `yum_repositories`.
* [`win`](./roles/win) added `win_acls` dict.
* [`users`](./roles/users) support all attributes of
  [`ansible.builtin.user`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/user_module.html).

## 0.2.11 ( 2022-10-25 )

* [`bootstrap`](./roles/bootstrap) extra list `bootstrap_packages_additional`
  and support for snaps via
  [`community.general.snap`](https://docs.ansible.com/ansible/2.9/modules/snap_module.html).
* [`files`](./roles/files/README.md) added module
  [`ansible.builtin.lineinfile`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/lineinfile_module.html)
  and added `backup` property to create backup file when making changes.
* [`server_update`](./roles/server_update/README.md) new role, moved from
  [`c2platform.mgmt`](https://gitlab.com/c2platform/ansible-collection-mgmt)
  collection.
* [`cacerts2`](./roles/cacerts2/README.md) removed obsolete / redundant code.
* [`win`](./roles/win/README.md) new dict `win_psexecs` to run commands as
  privileged user and various new win modules.
* [`facts`](./roles/facts/README.md) new var `common_facts_ignore_unreachable`
  to ignore unreachable hosts.
* [`restart`](./roles/restart/README.md) new var `restart_ignore_unreachable`
  and added new modules
  [`ansible.builtin.copy`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/copy_module.html)
  and
  [`ansible.windows.win_wait_for`](https://docs.ansible.com/ansible/latest/collections/ansible/windows/win_wait_for_module.html).
* [`mount`](./roles/mount/README.md) new role to configure / manage mount
  points. This replaces `nfs_mounts`.

## 0.2.10 ( 2022-10-12 )

* [`restart`](./roles/restart) `restart_hosts` list to select hosts.
* [`groups2items`](./README.md#groups2items) new filter.
* [`win`](./roles/win/README.md) added capability to manage files, directories
  using `win_files` and capability to create, manage scheduled tasks using
  `win_scheduled_tasks`.

## 0.2.9 ( 2022-10-02 )

* 0.2.8 → 0.2.9

## 0.2.8 ( 2022-10-02 )

* [`bootstrap`](./roles/bootstrap) improved deprecation message.
* [`common`](./roles/common) removed deprecated variable
  `common_pip_ca_trust_store`.
* [`files`](./roles/files) more robust ( don't swallow errors ).
* [`restart`](./roles/restart) new restart role.

## 0.2.7 ( 2022-09-19 )

* New / refactored / simplified [`bootstrap`](./roles/bootstrap) role. This new
  version deprecates variables `bootstrap_packages_extra`, `common_packages`,
  `common_packages_extra`, `common_pip_packages` and
  `common_pip_packages_extra`. You should only use `bootstrap_packages` now.
* Enhanced [`rest`](./roles/rest) role. The role now also allows you to
  configure `user`, `password`, `force_basic_auth`, `return_content`, `no_log`
  of the
  [`ansible.builtin.uri`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/uri_module.html).
* Enhanced [`cacerts2`](./roles/cacerts2) role so that it can also deploy CA
  files. Some applications require CA certificates / files during installation
  for example Harbor.

## 0.2.6 (2022-09-14)

* First release from GitLab

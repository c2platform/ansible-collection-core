from __future__ import absolute_import, division, print_function

__metaclass__ = type

from ansible.errors import AnsibleFilterError
import os
import hashlib
import base64


def inventory_hostname_group_index(inventory_hostname, group_name, groups):
    if group_name in groups:
        if inventory_hostname in groups[group_name]:
            return groups[group_name].index(inventory_hostname) + 1
        else:
            return None
    else:
        return None


def group_length(group_name, groups):
    if group_name in groups:
        return len(groups[group_name])
    else:
        return None


def inventory_hostname_vars(inventory_hostname, vars, default_vars=[]):
    if inventory_hostname in vars:
        return vars[inventory_hostname]
    else:
        return default_vars


class FilterModule(object):

    def filters(self):
        return {
            "inventory_hostname_group_index": inventory_hostname_group_index,
            "group_length": group_length,
            "inventory_hostname_vars": inventory_hostname_vars,
        }

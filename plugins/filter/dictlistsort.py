from __future__ import absolute_import, division, print_function

__metaclass__ = type

from ansible.errors import AnsibleFilterError
import os
import hashlib
import base64


def dictlistsort(groups):
    if not isinstance(groups, dict):
        raise AnsibleFilterError("Input should be a dictionary")

    sorted_groups = {}

    for key in sorted(groups.keys()):
        sorted_groups[key] = sorted(groups[key])

    return sorted_groups


class FilterModule(object):
    def filters(self):
        return {
            "dictlistsort": dictlistsort,
        }

from __future__ import absolute_import, division, print_function

__metaclass__ = type

from ansible.errors import AnsibleFilterError
from pprint import pprint  # TODO remove


def filter_value_from_facts(hostname, facts, filter):
    fct = None
    for fact in facts:
        if fact["item"] == hostname:
            if "ansible_os_family" in fact["ansible_facts"]:
                fct = fact["ansible_facts"]["ansible_os_family"]
    return fct


# Return service name
def restart_service_name(service_name, hostname, facts):  # e.g. apache2 or httpd
    sn = None
    if type(service_name) is str:
        return service_name
    fvff = filter_value_from_facts(hostname, facts, "ansible_os_family")
    if fvff:
        if fvff in service_name:
            sn = service_name[fvff]
        elif "default" in service_name:
            sn = service_name["default"]
    return sn


class FilterModule(object):

    def filters(self):
        return {
            "restart_service_name": restart_service_name,
        }

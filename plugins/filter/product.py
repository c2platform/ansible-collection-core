from __future__ import absolute_import, division, print_function

__metaclass__ = type

from ansible.errors import AnsibleFilterError


def product(list1, list2):
    result = []
    for item1 in list1:
        for item2 in list2:
            merged_item = dict(item1, **item2)
            result.append(merged_item)
    return result


class FilterModule(object):

    def filters(self):
        return {
            "product": product,
        }

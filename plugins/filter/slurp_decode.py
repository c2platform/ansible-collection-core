from __future__ import absolute_import, division, print_function

__metaclass__ = type

from ansible.errors import AnsibleFilterError
import base64


def slurp_decode(results):
    for result in results:
        decodedBytes = base64.urlsafe_b64decode(result["content"])
        result["content-decoded"] = str(decodedBytes, "utf-8")
    return results


class FilterModule(object):

    def filters(self):
        return {
            "slurp_decode": slurp_decode,
        }

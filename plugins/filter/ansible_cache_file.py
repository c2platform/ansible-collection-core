from __future__ import absolute_import, division, print_function

__metaclass__ = type

from ansible.errors import AnsibleFilterError
import os
import hashlib
import base64


def ansible_cache_file(url):
    bn = os.path.basename(url)
    ename = os.path.splitext(bn)[1]
    bn2 = os.path.splitext(bn)[0]
    url_hash = hashlib.sha1(url.encode("utf-8")).hexdigest()
    return os.path.join(os.path.sep, "/var/tmp", bn2 + "-" + url_hash + "." + ename)


class FilterModule(object):

    def filters(self):
        return {
            "ansible_cache_file": ansible_cache_file,
        }

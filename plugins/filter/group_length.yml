---
DOCUMENTATION:
  name: c2platform.core.group_length
  author: onknows
  version_added: "1.0"
  short_description: Get the number of hosts in a specified group.
  description:
    - >-
      This filter returns the number of hosts in a specified group contained
      in the groups dictionary. If the group is not found, it returns None.
  options:
    group_name:
      description: The name of the group to get the length for.
      type: str
      required: true
    groups:
      description: >-
        The dictionary of groups containing lists of inventory hostnames.
      type: dict
      required: true
  seealso:
    - plugin_type: filter
      plugin: ansible.builtin.length

RETURN:
  _value:
    description: >-
      The number of hosts in the specified group or None
      if the group is not found.
    type: int

EXAMPLES: |
  Consider the following groups dictionary:

  ```yaml
  groups:
    webservers:
      - host1
      - host2
      - host3
    dbservers:
      - host4
      - host5
  ```

  Applying the group_length filter:

  ```yaml
  webservers_count: "{{ groups | group_length('webservers') }}"
  ```

  Results in the count of hosts:

  ```yaml
  webservers_count: 3
  ```

  If the group is not found:

  ```yaml
  unknown_count: "{{ groups | group_length('unknown') }}"
  ```

  Results in:

  ```yaml
  unknown_count: None
  ```

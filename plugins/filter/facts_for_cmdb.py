"""facts_for_cmdb filters."""

from __future__ import absolute_import, division, print_function

__metaclass__ = type

DOCUMENTATION = r"""
    name: facts_for_cmdb
    author: onknows
    version_added: "1.0.13"
    short_description: Transforms Ansible facts for `ansible-cmdb`
    description:
      - This filter takes Ansible facts and transforms them to the format that can be consumed by `ansible-cmdb`.
      - It prefixes each fact with 'ansible_' for this purpose.
    options:
      ansible_facts:
        description: The original ansible facts dictionary.
        required: True
        type: dict
    examples:
      - name: Transform Ansible facts for CMDB
        description: Transform Ansible facts for easier integration with CMDB systems.
        code: |
          - name: Gather Ansible facts
            ansible.builtin.setup:

          - name: Transform facts for CMDB
            set_fact:
              transformed_facts: "{{ ansible_facts | c2platform.core.facts_for_cmdb }}"
    returns:
      ansible_facts:
        description: A dictionary with transformed ansible facts, prefixed with 'ansible_', ready for `ansible-cmdb` consumption.
        type: dict
        returned: always
"""

from ansible.errors import AnsibleFilterError
import os


def facts_for_cmdb(ansible_facts):
    facts = {}
    facts["ansible_facts"] = {}

    for key, value in ansible_facts.items():
        if key != "_ansible_facts_gathered":
            new_key = f"ansible_{key}"
            facts["ansible_facts"][new_key] = value

    return facts


class FilterModule(object):
    """facts_for_cmdb filters"""

    def filters(self):
        return {
            "facts_for_cmdb": facts_for_cmdb,
        }

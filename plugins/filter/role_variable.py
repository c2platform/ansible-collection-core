from __future__ import absolute_import, division, print_function

__metaclass__ = type

from ansible.errors import AnsibleFilterError
import os
import hashlib
import base64


def role_variable(resource_type, role_name):
    if role_name in resource_type:
        return resource_type
    else:
        return f"{role_name}_{resource_type}"


class FilterModule(object):
    def filters(self):
        return {
            "role_variable": role_variable,
        }

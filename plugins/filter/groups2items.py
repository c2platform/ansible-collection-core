from __future__ import absolute_import, division, print_function

__metaclass__ = type

from ansible.errors import AnsibleFilterError


def resources(dct_or_list):
    lst = []
    if isinstance(dct_or_list, list):
        for lst_item in dct_or_list:
            lst.append(resources_dict(lst_item))
        return lst
    else:
        return resources_dict(dct_or_list)


def resources_dict(dct):
    # Check if 'resources' key exists
    if "resources" in dct:
        resource_list = dct["resources"]
    else:
        # If 'resources' key does not exist, make dct a single-item list
        resource_list = [dct]

    # Check if 'defaults' key exists
    if "defaults" in dct:
        # Create a new list to store updated items
        updated_resource_list = []
        for item in resource_list:
            # Start with a copy of the defaults
            new_item = dct["defaults"].copy()
            # Update with the values from the item
            new_item.update(item)
            updated_resource_list.append(new_item)
        return updated_resource_list

    # Return the resource list
    # (either as originally found or as a single-item list)
    return resource_list


def groups2items(dct, group_name="group", process_resources=False):
    itms = []
    if not dct:  # dct is empty dict
        return itms
    if isinstance(dct, list):  # dct is a list
        itms = dct
    else:  # dict
        ky = next(iter(dct))  # get first key
        if isinstance(dct[ky], list) or isinstance(dct[ky], dict):
            # dct is dict of lists or dicts
            for grp, values in sorted(dct.items(), key=lambda item: item[0]):
                if isinstance(values, list):
                    for value in values:
                        value[group_name] = grp
                        itms.append({group_name: grp, **value})
                else:  # dict of dicts
                    values[group_name] = grp
                    itms.append({group_name: grp, **values})
        else:  # dct is not a dict of lists
            itms = [dct]  # one item list
    if process_resources:
        itms2 = []
        for itm in itms:
            grp = itm[group_name]
            itm2 = resources(itm)
            if isinstance(itm2, list):
                for itm3 in itm2:
                    itm3[group_name] = grp
                    itms2.append(itm3)
            else:
                itms2.append(itm2)
        itms = itms2
    return itms


class FilterModule(object):

    def filters(self):
        return {
            "groups2items": groups2items,
        }

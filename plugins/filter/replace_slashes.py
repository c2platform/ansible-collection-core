from __future__ import absolute_import, division, print_function

__metaclass__ = type

from ansible.errors import AnsibleFilterError
import os
import hashlib
import base64


def replace_slashes(path, replace_with="\\"):
    return path.replace("/", replace_with)


class FilterModule(object):
    def filters(self):
        return {
            "replace_slashes": replace_slashes,
        }

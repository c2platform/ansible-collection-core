from __future__ import absolute_import, division, print_function

__metaclass__ = type

from ansible.errors import AnsibleFilterError


def update_list_attribute(lst, key, value):
    for lst_itm in lst:
        if key in lst_itm:
            lst_itm[key] = value
    return lst


def update_list_attibute(lst, key, value):
    raise AnsibleFilterError(
        "The filter 'update_list_attibute' is deprecated due to a spelling error. "
        "Please use 'update_list_attribute' instead."
    )


class FilterModule(object):

    def filters(self):
        return {
            "update_list_attribute": update_list_attribute,
            "update_list_attibute": update_list_attibute,
        }

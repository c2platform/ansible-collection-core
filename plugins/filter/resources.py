"""ansible filters."""

from __future__ import absolute_import, division, print_function

__metaclass__ = type

from ansible.errors import AnsibleFilterError
import os
import hashlib
import base64


def resources(dct_or_list):
    lst = []
    if type(dct_or_list) == list:
        for lst_item in dct_or_list:
            lst.append(resources_dict(lst_item))
        return lst
    else:
        return resources_dict(dct_or_list)


def resources_dict(dct):
    # Check if 'resources' key exists
    if "resources" in dct:
        resource_list = dct["resources"]
    else:
        # If 'resources' key does not exist, make dct a single-item list
        resource_list = [dct]

    # Check if 'defaults' key exists
    if "defaults" in dct:
        # Create a new list to store updated items
        updated_resource_list = []
        for item in resource_list:
            # Start with a copy of the defaults
            new_item = dct["defaults"].copy()
            # Update with the values from the item
            new_item.update(item)
            updated_resource_list.append(new_item)
        return updated_resource_list

    # Return the resource list
    # (either as originally found or as a single-item list)
    return resource_list


class FilterModule(object):
    """ansible filters."""

    def filters(self):
        return {
            "resources": resources,
        }

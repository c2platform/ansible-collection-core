---
DOCUMENTATION:
  name: c2platform.core.add_attributes
  author: onknows
  version_added: "1.0"
  short_description: >-
    Add attributes from a dictionary to each item in a list of dictionaries.
  description:
    - >-
      This filter adds attributes from a specified dictionary to each item in a
      list of dictionaries. The filter updates each dictionary in the list with
      the key-value pairs from the provided dictionary.
    - >-
      This can be useful for augmenting a list of dictionaries with additional
      data in Ansible playbooks.
  options:
    lst:
      description: >-
        A list of dictionaries to which the attributes will be added.
      type: list
      elements: dict
      required: true
    dct:
      description: >-
        A dictionary containing the attributes to be added to each
        item in the list.
      type: dict
      required: true
  seealso:
    - plugin_type: filter
      plugin: ansible.builtin.combine

RETURN:
  _value:
    description: The updated list of dictionaries with the added attributes.
    type: list
    elements: dict

EXAMPLES: |
  Consider the following list of dictionaries and a dictionary of attributes:

  ```yaml
  list_of_items:
    - name: item1
      value: 100
    - name: item2
      value: 200
  ```

  ```yaml
  additional_attributes:
    category: electronics
    in_stock: true
  ```

  Applying the add_attributes filter:

  ```yaml
  updated_list: "{{ list_of_items | add_attributes(additional_attributes) }}"
  ```

  Results in the following updated list of dictionaries:

  ```yaml
  updated_list:
    - name: item1
      value: 100
      category: electronics
      in_stock: true
    - name: item2
      value: 200
      category: electronics
      in_stock: true
  ```

from __future__ import absolute_import, division, print_function

__metaclass__ = type

from ansible.errors import AnsibleFilterError


def add_attributes(lst, dct):
    for itm in lst:
        itm.update(dct)
    return lst


class FilterModule(object):

    def filters(self):
        return {
            "add_attributes": add_attributes,
        }

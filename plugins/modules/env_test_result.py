#!/usr/bin/python

from ansible.module_utils.basic import AnsibleModule

DOCUMENTATION = r'''
---
module: c2platform.core.env_test_result
short_description: Checks the status of a given port configuration
version_added: "1.0.0"
description:
  - This module examines a provided port configuration to determine if all messages have been received and if the port is marked as managed by Ansible.
  - It is useful for validating the status and operational health of network ports in an automated fashion.
options:
  port:
    description:
      - A dictionary containing the port's configuration, including message reception status and Ansible management flag.
    required: true
    type: dict
    suboptions:
      messages:
        description:
          - A sub-dictionary containing message-related flags.
        type: dict
        suboptions:
          all_messages_received:
            description:
              - A flag indicating whether all expected messages have been received on this port.
            type: bool
      ansible_managed:
        description:
          - A flag indicating if this port is managed by Ansible.
        type: bool
      failed:
        description:
          - A flag indicating if the port has encountered a failure.
        type: bool
      message:
        description:
          - A message providing details about the port's failure, if any.
        type: str

author:
  - Your Name (@yourgithub)

requirements:
  - A network device with configurable ports.
'''

EXAMPLES = r'''
- name: Check port status
  port_status_checker:
    port:
      ansible_managed: true
      failed: false
      messages:
        all_messages_received: true
'''

RETURN = r'''
changed:
  description: Indicates if the module made any changes to the system.
  returned: always
  type: bool
  sample: false
failed:
  description: Indicates if the module encountered any failure during its execution.
  returned: always
  type: bool
  sample: false
message:
  description: Provides a message detailing the module's execution outcome.
  returned: on failure
  type: str
  sample: "Server did not receive all expected messages."
'''

from ansible.module_utils.basic import AnsibleModule

def run_module():
    module_args = dict(
        port=dict(type='dict', required=True),
    )

    result = dict(
        changed=False,
        failed=False,
        message='',
    )

    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    if module.check_mode:
        module.exit_json(**result)

    port = module.params['port']
    all_messages_received = port.get('messages', {}).get('all_messages_received', False)
    if not all_messages_received and port['ansible_managed']:
        module.fail_json(msg="Server did not receive all expected messages", **result)

    if port['failed'] == True:
        module.fail_json(msg=port['message'], **result)

    module.exit_json(**result)

def main():
    run_module()

if __name__ == '__main__':
    main()

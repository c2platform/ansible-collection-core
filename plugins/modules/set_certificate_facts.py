#!/usr/bin/python

from ansible.module_utils.basic import *


def deploy_path(data, crt, ext):
    return "{}/{}-{}.{}".format(crt['deploy'][ext]['dir'], crt['common_name'],
                                data['hostname'], ext)

def download_path(data):
    return os.path.join(ca_dir(data), 'downloads')

def src_path_csp(data, crt, ext):
    path = None
    csp_ext = data['ca_domain']['external_csp_tag']
    if csp_ext:
        path_csp = "{}/{}-{}.{}.{}".format(crt['dir'], crt['common_name'],
                                data['hostname'], csp_ext, ext)
        if os.path.exists(path_csp):
            path = path_csp
    return path

def src_path(data, crt, ext):
    path = "{}/{}-{}.{}".format(crt['dir'], crt['common_name'],
                                data['hostname'], ext)
    return path

def src_path_trusted_cert(data, trusted_cert):
    file_name = "{}.{}".format(trusted_cert['alias'], 'crt')
    return os.path.join(download_path(data), file_name)

def ca_path(data, ext):
    return "{}/{}.{}".format(ca_dir(data),
                             data['ca_domain']['common_name'], ext)


def ca_dir(data):
    return "{}/{}".format(data['ca_dir'], data['ca_domain']['common_name'])


def certificates_key(data):
    return "{}_{}".format(data['role_name'], 'cacerts2_certificates')


def facts_ca(data, facts):
    facts['cacerts2_ca_domain'] = data['ca_domain']
    facts['cacerts2_ca_domain']['dir'] = ca_dir(data)
    facts['cacerts2_ca_domain']['downloads'] = download_path(data)
    facts['cacerts2_ca_domain']['key'] = ca_path(data, 'key')
    facts['cacerts2_ca_domain']['crt'] = ca_path(data, 'crt')
    facts['cacerts2_ca_domain']['csr'] = ca_path(data, 'csr')
    facts['cacerts2_ca_domain']['crl'] = ca_path(data, 'crl')
    return facts


def facts_certificates(data, facts):
    if data['role_name']:
        ck = certificates_key(data)
        facts[ck] = []
        for crt in data['certificates']:
            facts[ck].append(cert(data, crt))
    return facts


def facts(data):
    facts = {}
    facts = facts_certificates(data, facts)
    facts = facts_ca(data, facts)
    #if data['role_name']:
    #    facts['cacerts2_certificates'] = facts[certificates_key(data)]
    return (False, facts)


def truststore_certs(data,crt):
    crts = []
    if 'certs' in crt['deploy']['truststore']:
        crts = crt['deploy']['truststore']['certs']
    elif 'trusted_certs' in data['ca_domain']:
        crts = data['ca_domain']['trusted_certs']
    return crts

def cert(data, crt):
    crt['dir'] = "{}/{}".format(ca_dir(data), data['role_name'])
    crt['create'] = {}
    for ext in crt['deploy']:
        if 'dest' in crt['deploy'][ext]:
            dest = crt['deploy'][ext]['dest']
            crt['deploy'][ext]['dir'] = os.path.dirname(dest)
        else:
            dest = deploy_path(data, crt, ext)
        if not 'dest' in crt['deploy'][ext]:
            crt['deploy'][ext]['dest'] = dest
        if ext == 'truststore':
            trusted_certs = truststore_certs(data,crt)
            for trusted_cert in trusted_certs:
                crt['create'][trusted_cert['alias']] = \
                    src_path_trusted_cert(data,trusted_cert)
            crt['deploy'][ext]['certs'] = trusted_certs
            crt['deploy'][ext]['create'] = False
    for ext in data['ca_domain']['create']:
        path = src_path(data, crt, ext)
        path_csp = src_path_csp(data, crt, ext)
        if path_csp:
            crt['deploy'][ext]['external-csp'] = True
            crt['create'][ext] = path_csp
        else:
            crt['create'][ext] = path
    return crt

# def debug_log(msg):
#     with open("/vagrant/python.log", "a") as myfile:
#         myfile.write(msg + "\n")

def main():
    fields = {"certificates": {"required": False, "type": "list"},
              "ca_server": {"required": True, "type": "str"},
              "ca_dir": {"required": True, "type": "str"},
              "hostname": {"required": True, "type": "str"},
              "ca_dir": {"required": True, "type": "str"},
              "role_name": {"required": False, "type": "str"},
              "ca_domain": {"required": True, "type": "dict"}}
    module = AnsibleModule(
        argument_spec=fields,
        supports_check_mode=True)
    has_changed, fcts = facts(module.params)
    module.exit_json(changed=has_changed, ansible_facts=fcts)


if __name__ == '__main__':
    main()

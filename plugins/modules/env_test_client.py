#!/usr/bin/python

from ansible.module_utils.basic import AnsibleModule
import socket
import sys

DOCUMENTATION = r'''
---
module: c2platform.core.env_test_client
short_description: Sends a message over a socket and expects a specific response
version_added: "1.0.0"
description:
  - This module sends a custom message to a specified host and port, expecting a specific response within a given timeout period.
  - It is useful for testing simple TCP-based services and protocols for availability and response accuracy.
options:
  host:
    description:
      - The target host IP or hostname where the message will be sent.
    required: true
    type: str
  port:
    description:
      - The target port on the host to which the message will be sent.
    required: true
    type: int
  message:
    description:
      - The message to be sent to the target host and port.
    required: true
    type: str
  expected_response:
    description:
      - The response expected from the target upon sending the message.
    required: true
    type: str
  timeout_seconds:
    description:
      - The timeout in seconds for waiting for the expected response.
    required: true
    type: int

author:
  - onknows

requirements:
  - A reachable network host and port.
'''

EXAMPLES = r'''
- name: Test custom socket communication
  custom_socket_comm:
    host: "192.168.1.100"
    port: 8080
    message: "Hello, server!"
    expected_response: "Hello, client!"
    timeout_seconds: 10
'''

RETURN = r'''
original_message:
  description: The original message that was sent to the target host.
  returned: always
  type: str
  sample: "Hello, server!"
response:
  description: The actual response received from the target host.
  returned: when successful
  type: str
  sample: "Hello, client!"
finding:
  description: The result of the message transmission and response evaluation.
  returned: always
  type: str
  sample: "Message was sent and correct response was received."
'''

def run_module():
    module_args = dict(
        host=dict(type='str', required=True),
        port=dict(type='int', required=True),
        message=dict(type='str', required=True),
        expected_response=dict(type='str', required=True),
        timeout_seconds=dict(type='int', required=True),
    )

    result = dict(
        changed=False,
        failed=False,
        original_message='',
        response='',
        finding='',
    )

    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    if module.check_mode:
        module.exit_json(**result)

    host = module.params['host']
    port = module.params['port']
    message = module.params['message']
    expected_response = module.params['expected_response']
    timeout_seconds = module.params['timeout_seconds']

    result['original_message'] = message

    # Set up the socket and connect to the server
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        try:
            s.settimeout(timeout_seconds)
            s.connect((host, port))
            s.sendall(message.encode('utf-8'))
            # s.sendall("whatever".encode('utf-8'))
            response = s.recv(1024).decode('utf-8')
            result['response'] = response

            if response == expected_response:
                result['finding'] = "Message was sent and correct response was received."
            else:
                result['finding'] = "Message was sent but not the correct response was received."
                result['failed'] = True

        except socket.timeout:
            result['finding'] = "Timeout waiting for response. The port may be filtered or the server is not responding."
            result['failed'] = True
        except ConnectionRefusedError:
            result['finding'] = "Connection refused. The port is likely closed."
            result['failed'] = True
        except Exception as e:
            result['finding'] = "A message could not be sent. This could be due to various reasons including network issues, server configuration, or the port being filtered: " + str(e)
            result['failed'] = True

    module.exit_json(**result)

def main():
    run_module()

if __name__ == '__main__':
    main()

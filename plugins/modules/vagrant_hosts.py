#!/usr/bin/python

from ansible.module_utils.basic import AnsibleModule
import yaml
import os


def read_vagrantfile(vagrantfile_path):
    with open(vagrantfile_path, "r") as file:
        return yaml.safe_load(file)


def generate_hosts_content(vagrant_data):
    default_prefix = vagrant_data["defaults"]["prefix"]
    domain = vagrant_data["defaults"]["domain"]
    hosts_content = {}

    for node in vagrant_data["nodes"]:
        # Use node-specific prefix if available, otherwise use default
        prefix = node.get("prefix", default_prefix)
        hostname = f"{prefix}-{node['name']}"
        fqdn = f"{hostname}.{domain}"
        line = f"{node['ip-address']} {hostname} {fqdn}"

        if "aliases" in node:
            line += " " + " ".join(node["aliases"])

        # Add line to the appropriate prefix in the hosts_content dictionary
        if prefix not in hosts_content:
            hosts_content[prefix] = []
        hosts_content[prefix].append(line)

    # Convert lists to single strings if needed
    for key in hosts_content:
        hosts_content[key] = "\n".join(hosts_content[key])

    return hosts_content


def find_existing_file(paths):
    if isinstance(paths, str):
        paths = [paths]
    for path in paths:
        if os.path.isfile(path):
            return path
    return None


def get_directory_contents(paths):
    contents = {}
    for path in paths:
        directory = os.path.dirname(path)
        try:
            contents[directory] = os.listdir(directory)
        except FileNotFoundError:
            contents[directory] = "Directory not found"
        except NotADirectoryError:
            contents[directory] = "Not a directory"
        except PermissionError:
            contents[directory] = "Permission denied"
    return contents


def run_module():
    module_args = dict(
        vagrantfile_yml=dict(type="raw", required=True),
    )

    result = dict(
        changed=False,
        vagrant_hosts_content="",
    )

    module = AnsibleModule(argument_spec=module_args, supports_check_mode=True)

    if module.check_mode:
        module.exit_json(**result)

    vagrantfile_paths = module.params["vagrantfile_yml"]
    vagrantfile_path = find_existing_file(vagrantfile_paths)

    if not vagrantfile_path:
        directory_contents = get_directory_contents(vagrantfile_paths)
        error_message = (
            "No valid Vagrantfile.yml found in the provided paths: {}\n"
            "Directory contents:\n".format(", ".join(vagrantfile_paths))
        )
        for directory, contents in directory_contents.items():
            error_message += "{}:\n{}\n".format(directory, "\n".join(contents))

        module.fail_json(msg=error_message, **result)

    try:
        vagrant_data = read_vagrantfile(vagrantfile_path)
        hosts_content = generate_hosts_content(vagrant_data)
        facts = {}
        facts["vagrant_hosts_content"] = hosts_content
        module.exit_json(ansible_facts=facts)
    except Exception as e:
        module.fail_json(msg=str(e), **result)


def main():
    run_module()


if __name__ == "__main__":
    main()

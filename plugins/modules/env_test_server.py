#!/usr/bin/python

import json
from ansible.module_utils.basic import AnsibleModule
import socket
import time
import select

DOCUMENTATION = r'''
---
module: c2platform.core.env_test_server
short_description: Listens for specific messages on multiple sockets and manages port states
version_added: "1.0.0"
description:
  - This module listens on specified ports for defined messages, responds back with a custom message, and manages port states. It supports checking if ports are open, opening ports, and handling incoming messages with configurable timeouts.
options:
  timeout_seconds:
    description: The maximum time in seconds the listener should wait for all messages across all ports.
    required: true
    type: int
  port:
    description: Port for the listener to start and receives messages on
    required: true
    type: int
  server:
    description: A dict specifying server details where the socket listener is running.
    required: true
    type: dict
    suboptions:
      node:
        description: The identifier for the node or server on which the listener is running.
        required: true
        type: str
      address:
        description: The IP address or hostname of the server.
        required: false
        default: 'localhost'
        type: str
  expected_messages:
    description: A list of messages the listener expects to receive.
    required: true
    type: list
    elements: str
  response_message:
    description: The message to send back upon receiving an expected message.
    required: false
    default: pong
    type: str
  log_file:
    description: Optional path to a file where every received message and significant events will be logged. Supports dynamic naming using server node and port information.
    required: false
    type: str
author:
  - onknows
'''

EXAMPLES = r'''
- name: Listen for messages on a server and manage port
  my_socket_listener:
    timeout_seconds: 10
    server:
      node: server1
      address: 192.168.1.100
    port: 8061
    expected_messages:
      - message1
      - message2
    response_message: pong
    log_file: /vagrant/logs/env_test_listener_{node}_{port}.log
'''

RETURN = r'''
port:
  description: Details about the port that was listened on, including management status and any errors encountered.
  returned: always
  type: dict
  contains:
    ansible_managed:
      description: Indicates if the port is managed by Ansible.
      returned: always
      type: bool
    failed:
      description: Whether any operation on the port failed.
      returned: always
      type: bool
    message:
      description: A message about the outcome of the port operation.
      returned: always
      type: str
    error:
      description: Any error messages encountered during the port operation.
      returned: when failed is true
      type: str
    received_messages:
      description: The list of messages that were received and matched the expected messages.
      returned: always
      type: list
      elements: str
messages:
  description: Details about the message handling process, including success or failure to receive all expected messages.
  returned: when socket is present and messages are processed
  type: dict
  contains:
    received_messages:
      description: The list of messages that were received and matched the expected messages.
      returned: always
      type: list
      elements: str
    all_messages_received:
      description: Whether all expected messages were received.
      returned: always
      type: bool
    message:
      description: A message about the outcome of the message handling operation.
      returned: always
      type: str
    error:
      description: Any error messages encountered during the message handling operation.
      returned: when there is an error
      type: str
'''

def debug(log_file, message, a_w='a'):
    if log_file is None:
        return
    with open(log_file, a_w) as f:
        f.write(message + '\n')

def check_port_open(port):
    """Check if a port is open on localhost."""
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        return s.connect_ex(('localhost', port)) == 0

def open_port(port):
    """Attempt to open a port and return the socket object, a boolean indicating success, and an error message if any."""
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.bind(('', port))
        s.listen()
        return s, True, None  # No error, so the last element is None
    except Exception as e:
        if s is not None:
            s.close()  # Close the socket if an exception occurs after it's created
        return None, False, str(e)

def handle_port(port, timeout_seconds, expected_messages, response_message, log_file):
    """Handle a single port according to its management flag and expected behavior."""
    result = {}
    result['port'] = port
    result['failed'] = False
    result['message'] = ''
    result['error'] = ''

    is_open = check_port_open(port)
    socket = None

    if is_open:
        # Port should not be open
        result['failed'] = True
        result['message'] = 'Port is already open. Ansible cannot open it!'
    else:
        # Try to open the port, if fails, set failed to true
        socket, opened,msg = open_port(port)
        if not opened:
            result['failed'] = True
            result['message'] = 'Failed to open port. Ansible cannot manage it!'
            result['error'] = msg
            if socket is not None:
                socket.close()  # Close the socket if it was opened but an error occurred afterward

    debug(log_file, f"Port {result['port']}: {result['message']}")
    if result['failed']:
        debug(log_file, '   '+ result['error'])

    return result, socket

def handle_messages(socket, timeout_seconds, expected_messages, response_message, log_file):
    try:
      result = {}
      result['received_messages'] = []
      result['failed'] = False
      result['message'] = ''
      result['error'] =''
      result['all_messages_received'] = False
      # debug(log_file, f"Expected_messages: {', '.join(expected_messages)}")
      start_time = time.time()
      while True:
          current_time = time.time()
          elapsed = current_time - start_time
          debug(log_file, f"Elapsed: {elapsed}")
          if elapsed >= timeout_seconds:
              result['message'] = "Timeout reached without receiving all messages"
              result['failed'] = False
              debug(log_file, result['message'])
              socket.close()
              return result
          # if not 'received_messages' in result:
          #     result['received_messages'] = []
          socket_timeout = timeout_seconds-elapsed
          debug(log_file, f"Waiting for message with timeout: {socket_timeout}")
          socket.settimeout(socket_timeout)
          try:
              conn, addr = socket.accept()
              with conn:
                  message = conn.recv(1024).decode('utf-8')
                  message = message.replace('\n', '').replace('\r','')
                  debug(log_file, f"Received message: {repr(message)}")
                  if message in expected_messages and message not in result['received_messages']:
                      result['received_messages'].append(message)
                      start_time = time.time()  # reset elapsed
                      debug(log_file, f"start_time TODO")
                      conn.sendall(response_message.encode('utf-8'))
                  if len(result['received_messages']) == len(expected_messages):
                      result['all_messages_received'] = True
                      result['message'] = "All expected messages received"
                      debug(log_file, 'All messages received')
                      socket.close()
                      return result
                  else:
                      debug(log_file, 'received_messages: '+ \
                            ', '.join(result['received_messages']))
          except Exception as e:
              debug(log_file, f'Error waiting for messages: {e}')
              continue
    finally:
        socket.close()

def run_module():
    module_args = dict(
        timeout_seconds=dict(type='int', required=True),
        port=dict(type='int', required=True),
        server=dict(type='dict', required=True),
        expected_messages=dict(type='list', required=True, elements='str'),
        response_message=dict(type='str', required=False, default='pong'),
        log_file=dict(type='str', required=False)
        # log_file=dict(type='str', required=False, default='/var/log/env_test_listener_{node}_{port}.log')
    )

    result = dict(
        changed=False,
        messages=[],
        port={},
    )

    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    if module.check_mode:
        module.exit_json(**result)

    try:
      timeout_seconds = module.params['timeout_seconds']
      port = module.params['port']
      expected_messages = module.params['expected_messages']
      response_message = module.params['response_message']
      server=module.params['server']
      log_file = module.params['log_file']
      if log_file:
          log_file = module.params['log_file'].format(node = server['node'], port = port )
      result, socket = handle_port(port, timeout_seconds, expected_messages, response_message, log_file)
      if socket and not result['failed']:
          msg = (f'Start listener on {str(port)} waiting for '
            f'messages: {", ".join(expected_messages)}')
          debug(log_file,msg, 'w')
          result = handle_messages(socket, timeout_seconds, expected_messages, response_message, log_file)
          # debug(log_file,"CLOSING SOCKET ( AGAIN )")
          socket.close()
    except Exception as e:
        debug(log_file, f'Error running module: {e}')

    # debug(log_file,"EXIT MODULE")
    module.exit_json(**result)

def main():
    run_module()

if __name__ == '__main__':
    main()

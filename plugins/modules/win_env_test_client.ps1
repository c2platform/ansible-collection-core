#Requires -Module Ansible.ModuleUtils.Legacy
#AnsibleRequires -OSVersion 6.2
#AnsibleRequires -CSharpUtil Ansible.Basic

<#
.SYNOPSIS
Sends a message over a socket and expects a specific response.

.DESCRIPTION
This module sends a custom message to a specified host and port, expecting a specific response within a given timeout period.
It is useful for testing simple TCP-based services and protocols for availability and response accuracy.

.PARAMETER host
The target host IP or hostname where the message will be sent.

.PARAMETER port
The target port on the host to which the message will be sent.

.PARAMETER message
The message to be sent to the target host and port.

.PARAMETER expected_response
The response expected from the target upon sending the message.

.PARAMETER timeout_seconds
The timeout in seconds for waiting for the expected response.

.EXAMPLE
PS> .\env_test_client_win.ps1 -host "192.168.1.100" -port 8080 -message "Hello, server!" -expected_response "Hello, client!" -timeout_seconds 10
#>

$spec = @{
    options = @{
        host = @{
            type = "str"
            required = $true
        }
        port = @{
            type = "int"
            required = $true
        }
        message = @{
            type = "str"
            required = $true
        }
        expected_response = @{
            type = "str"
            required = $true
        }
        timeout_seconds = @{
            type = "int"
            required = $true
        }
    }
}

$module = [Ansible.Basic.AnsibleModule]::Create($args, $spec)
$ErrorActionPreference = "Stop"
Set-StrictMode -Version 2.0

$hostname = $module.Params.host
$port = $module.Params.port
$expected_response = $module.Params.expected_response
$timeout_seconds = $module.Params.timeout_seconds
$message = $module.Params.message

$result = @{
    changed = $false
    failed = $false
    original_message = $message
    response = ""
    finding = ""
}

try {
    $tcpClient = New-Object System.Net.Sockets.TcpClient
    $tcpClient.Connect($hostname, $port)
    $tcpClient.ReceiveTimeout = $timeout_seconds * 1000

    $stream = $tcpClient.GetStream()
    $writer = New-Object System.IO.StreamWriter $stream
    $reader = New-Object System.IO.StreamReader $stream

    $writer.WriteLine($message)
    $writer.Flush()

    $response = $reader.ReadLine()
    $result.response = $response

    if ($response -eq $expected_response) {
        $result.finding = "Message was sent and correct response was received."
    } else {
        $result.finding = "Message was sent but not the correct response was received."
    }

    $tcpClient.Close()
}
catch [System.Net.Sockets.SocketException] {
    $result.finding = "Connection refused. The port is likely closed."
}
catch [System.TimeoutException] {
    $result.finding = "Timeout waiting for response. The port may be filtered or the server is not responding."
}
catch {
    $result.finding = "A message could not be sent. This could be due to various reasons including network issues, server configuration, or the port being filtered: $_"
}

$module.Result.changed = $result.changed
$module.Result.failed = $result.failed
$module.Result.original_message = $result.original_message
$module.Result.response = $result.response
$module.Result.finding = $result.finding

$module.ExitJson()